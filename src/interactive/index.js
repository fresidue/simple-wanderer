'use strict';

const _ = require('lodash');

const {
  ORDERED_COMMANDS,
  COMMAND_INDICES,
  ORIENTATIONS,
} = require('../constants');

const {
  runSimulation,
  updateSimulation,
  // clearSimulation,
} = require('../simulation');

const {
  TITLE,
  COLORS,
  TILE_CHARS,
  TILE_COLORS,
  ACTION_NAME_BY_COMMAND,
  // COMMAND_BY_ACTION_NAME,
  BOARD_NUMBER_SPACING,
  STATE_EVENT_EMITTER,
  STATE_EVENT_TYPES,
  MAIN_VIEW_TYPES,
} = require('./constants');

const {
  generateBoardNumbers,
  DIMENSIONS,
} = require('./utils');

const {createScreen} = require('./screen');
const {
  createIntroView,
  createBoardView,
} = require('./views');

const createBackground = ({blessed}) => {
  const background = blessed.box({
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    style: {
      fg: COLORS.background,
      bg: COLORS.background,
    },
  });
  return background;
};

const createTitleBar = ({blessed}) => {
  const titleBar = blessed.box({
    left: DIMENSIONS.titleBar.left,
    top: 0,
    width: Math.max(..._.map(TITLE.split('\n'), (line) => line.length)),
    height: DIMENSIONS.titleBar.height,
    content: TITLE,
    style: {
      fg: COLORS.titleForeground,
      bg: COLORS.background,
      hover: {
        bg: COLORS.backgroundHover,
      },
    },
  });
  return titleBar;
};


// MUTABLE state
const state = {
  viewType: MAIN_VIEW_TYPES.intro,
};


const initializeInteractive = async (input) => {

  // init - create primary views & attach to background
  const {blessed, screen} = createScreen();
  const background = createBackground({blessed});
  screen.append(background);
  const titleBar = createTitleBar({blessed});
  background.append(titleBar);



  // console.log('creating prompt');
  // const prompt = blessed.prompt({
  //   left: 2,
  //   top: 10,
  //   height: 20,
  //   width: 100,
  //
  //   ileft: 2,
  //
  //   border: 'line',
  //   style: {
  //     fg: 'blue',
  //     bg: 'red',
  //     bold: true,
  //     border: {
  //       bg: 'red',
  //     },
  //   },
  // });
  // screen.append(prompt);
  // screen.render();
  // prompt.input('Search:', 'test', function() {});
  // console.log('rendered prompt');



  // let mainView = createBoardView({blessed, screen, state: state.simState});
  let mainView = createIntroView({blessed, screen, input});
  background.append(mainView);
  screen.render();



  STATE_EVENT_EMITTER.on(STATE_EVENT_TYPES.view, ({viewType, simState}) => {
    state.viewType = viewType;
    background.remove(mainView);
    switch (viewType) {
      case MAIN_VIEW_TYPES.intro: {
        throw new Error('DevErr: we should never switch to introView');
      }
      case MAIN_VIEW_TYPES.editBoard: {
        throw new Error('MAIN_VIEW_TYPES.editBoard not yet implemented');
      }
      case MAIN_VIEW_TYPES.board: {
        mainView = createBoardView({blessed, screen, state: simState});
        background.append(mainView);
        break;
      }
      default: {
        // console.log('nope.. in default:', {MAIN_VIEW_TYPES});
        throw new Error(`DevErr: unhandled state.viewType: ${viewType}`);
      }
    }
    screen.render();
  });

  // back

  background.key([COMMAND_INDICES.quit], async () => {
    if (state.viewType === MAIN_VIEW_TYPES.board) {
      const nextState = await updateSimulation(`${COMMAND_INDICES.quit}`);
      mainView.refresh(nextState);
    }
  });
  background.key([COMMAND_INDICES.moveForward, 'up'], async () => {
    if (state.viewType === MAIN_VIEW_TYPES.board) {
      const nextState = await updateSimulation(`${COMMAND_INDICES.moveForward}`);
      mainView.refresh(nextState);
    }
  });
  background.key([COMMAND_INDICES.moveBackward, 'down'], async () => {
    if (state.viewType === MAIN_VIEW_TYPES.board) {
      const nextState = await updateSimulation(`${COMMAND_INDICES.moveBackward}`);
      mainView.refresh(nextState);
    }
  });
  background.key([COMMAND_INDICES.rotateCW, 'right'], async () => {
    if (state.viewType === MAIN_VIEW_TYPES.board) {
      const nextState = await updateSimulation(`${COMMAND_INDICES.rotateCW}`);
      mainView.refresh(nextState);
    }
  });
  background.key([COMMAND_INDICES.rotateCCW, 'left'], async () => {
    if (state.viewType === MAIN_VIEW_TYPES.board) {
      const nextState = await updateSimulation(`${COMMAND_INDICES.rotateCCW}`);
      mainView.refresh(nextState);
    }
  });
  background.key([COMMAND_INDICES.boardRotateCW, 's'], async () => {
    if (state.viewType === MAIN_VIEW_TYPES.board) {
      const nextState = await updateSimulation(`${COMMAND_INDICES.boardRotateCW}`);
      mainView.refresh(nextState);
    }
  });
  background.key([COMMAND_INDICES.boardRotateCCW, 'a'], async () => {
    if (state.viewType === MAIN_VIEW_TYPES.board) {
      const nextState = await updateSimulation(`${COMMAND_INDICES.boardRotateCCW}`);
      mainView.refresh(nextState);
    }
  });

};

module.exports = {
  initializeInteractive,
};
