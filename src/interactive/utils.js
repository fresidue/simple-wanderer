'use strict';

const _ = require('lodash');
const {BOARD_NUMBER_SPACING} = require('./constants');

// ////
// DIMENSIONS - pseudo-constant
// ////

const DIMENSIONS = {
  // top
  titleBar: {height: 5, left: 3},
  // board
  actionButton: {height: 3, width: 8},
  tile: {width: 5, height: 3},
  leftNumbersContainer: {
    width: (bounds) => {
      const yNumbers = generateBoardNumbers(bounds).y;
      const lengths = _.map(yNumbers, (yNumber) => yNumber.length);
      const width = Math.max(...lengths);
      return width;
    },
  },
  // intro
  inputLine: {height: 1},
  parsedInputBox: {height: 6},
  managedInputBox: {height: 8},
  parseErrorBox: {height: 1},
  editMenu: {height: 21}, // is really composed of 3 rows..
  removalBoardButton: {width: 5, height: 3},
};

// intro
DIMENSIONS.editRemovalsBox = {
  top: ({parsedInput}) => DIMENSIONS.inputLine.height + (parsedInput ? DIMENSIONS.parsedInputBox.height : DIMENSIONS.parseErrorBox.height) + DIMENSIONS.managedInputBox.height + DIMENSIONS.editMenu.height,
  height: ({width, height}) => !width || !height ? 1 : DIMENSIONS.removalBoardButton.height * height,
};
// DIMENSIONS.finishButton = {
//   top: ({parsedInput, width, height}) => DIMENSIONS.editRemovalsBox.top({parsedInput}) + DIMENSIONS.editRemovalsBox.height({width, height}),
//   height: 3,
//   width: 20,
// },
// board
DIMENSIONS.boardContainer = {
  width: (bounds) => (bounds.diffX + 3) * DIMENSIONS.tile.width,
  height: (bounds) => (bounds.diffY + 3) * DIMENSIONS.tile.height,
};


// ////
// other junk
// ////

const generateBoardNumbers = (bounds) => {
  const numX = Math.floor(bounds.diffX / BOARD_NUMBER_SPACING) + 1;
  const numY = Math.floor(bounds.diffY / BOARD_NUMBER_SPACING) + 1;
  const boardNumbers = {
    x: _.times(numX, (index) => String(bounds.minX + BOARD_NUMBER_SPACING * index)),
    y: _.times(numY, (index) => String(bounds.minY + BOARD_NUMBER_SPACING * index)),
  };
  return boardNumbers;
};

const getBoardBounds = (board) => {
  const initialTile = _.values(board.tiles).pop();
  let minX = initialTile.x;
  let minY = initialTile.y;
  let maxX = initialTile.x;
  let maxY = initialTile.y;
  _.each(board.tiles, (tile) => {
    minX = Math.min(minX, tile.x);
    minY = Math.min(minY, tile.y);
    maxX = Math.max(maxX, tile.x);
    maxY = Math.max(maxY, tile.y);
  });
  const bounds = {minX, minY, maxX, maxY, diffX: maxX - minX, diffY: maxY - minY};
  return bounds;
};

module.exports = {
  DIMENSIONS,
  generateBoardNumbers,
  getBoardBounds,
};
