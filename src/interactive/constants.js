'use strict';

const _ = require('lodash');
const EventEmitter = require('events');
const {COMMANDS} = require('../constants');

const STATE_EVENT_EMITTER = new EventEmitter();
const STATE_EVENT_TYPES = {
  update: 'update',
  view: 'view',
};

const MAIN_VIEW_TYPES = {
  board: 'board',
  intro: 'intro',
  editBoard: 'editBoard',
};

const TITLE = `
   ss iii m m ppp l   eee   w w aaa n n ddd eee rrr eee rrr
   s   i  mmm ppp l   ee    www aaa nnn d d ee  rr  ee  rr
  ss  iii m m p   lll eee   w w a a n n ddd eee r r eee r r
`;

const COLORS = {
  background: 'black',
  backgroundHover: '#000209',

  titleForeground: 'blue',
  boundsForeground: 'yellow',

  actionButtonForeground: 'red',
  actionButtonBackground: '#000209',
  actionButtonBackgroundHover: '#323523',

  boardBackgroundEmpty: '#3d523c',
  boardBackgroundEmptyHover: '#182118',

  stateBoxForeground: 'white',
};

const TILE_CHARS = {
  north: '▲',
  east: '⯈',
  south: '▼',
  west: '⯇',
  death: '☠',
  safe: '◼',
  bomb: 'X',
};

const TILE_COLORS = {
  alive: 'green',
  death: 'white', // not relevant with skull?
  quit: 'red',
  safe: 'white',
  trodden: 'yellow',
  bomb: 'blue',
};

const ACTION_NAME_BY_COMMAND = {
  [COMMANDS.quit]: 'quit',
  [COMMANDS.moveForward]: 'moveF',
  [COMMANDS.moveBackward]: 'moveB',
  [COMMANDS.rotateCW]: 'rotCW',
  [COMMANDS.rotateCCW]: 'rotCCW',
  [COMMANDS.boardRotateCW]: 'brdCW',
  [COMMANDS.boardRotateCCW]: 'brdCCW',
};

const COMMAND_BY_ACTION_NAME = _.reduce(ACTION_NAME_BY_COMMAND, (acc, name, command) => {
  acc[name] = command;
  return acc;
}, {});

const BOARD_NUMBER_SPACING = 5;


module.exports = {
  TITLE,
  COLORS,
  TILE_CHARS,
  TILE_COLORS,
  ACTION_NAME_BY_COMMAND,
  COMMAND_BY_ACTION_NAME,
  BOARD_NUMBER_SPACING,
  STATE_EVENT_EMITTER,
  STATE_EVENT_TYPES,
  MAIN_VIEW_TYPES,
};
