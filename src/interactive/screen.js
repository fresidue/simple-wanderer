'use strict';


const createScreen = () => {

  // this is the ONLY require for "blessed"
  // which should NOT be required when NOT running interactive
  const blessed = require('blessed');

  const screen = blessed.screen({
    smartCSR: true,
  });
  screen.title = 'simple wanderer';

  // Quit on Escape, q, or Control-C.
  screen.key(['escape', 'q', 'C-c'], (ch, key) => {
    process.exit(0);
  });

  return {blessed, screen};
};


module.exports = {
  createScreen,
};
