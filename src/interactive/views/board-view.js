'use strict';

const _ = require('lodash');

const {
  ORDERED_COMMANDS,
  COMMAND_INDICES,
  ORIENTATIONS,
} = require('../../constants');

const {
  runSimulation,
  updateSimulation,
  // clearSimulation,
} = require('../../simulation');

const {
  TITLE,
  COLORS,
  TILE_CHARS,
  TILE_COLORS,
  ACTION_NAME_BY_COMMAND,
  // COMMAND_BY_ACTION_NAME,
  BOARD_NUMBER_SPACING,
  STATE_EVENT_EMITTER,
  STATE_EVENT_TYPES,
} = require('../constants');

const {
  DIMENSIONS,
  generateBoardNumbers,
  getBoardBounds,
} = require('../utils');


const createBackground = ({blessed}) => {
  const background = blessed.box({
    left: 0,
    top: DIMENSIONS.titleBar.height,
    width: '100%',
    height: '100%',
    style: {
      bg: COLORS.background,
    },
  });
  return background;
};

const createActionButton = ({blessed, command, index = 0}) => {
  const actionName = ACTION_NAME_BY_COMMAND[command];
  const actionButton = blessed.box({
    left: 1 + DIMENSIONS.titleBar.left + index * (DIMENSIONS.actionButton.width + 1),
    top: -1,
    width: DIMENSIONS.actionButton.width,
    height: DIMENSIONS.actionButton.height,
    content: `\n ${actionName}`,
    style: {
      fg: COLORS.actionButtonForeground,
      bg: COLORS.actionButtonBackground,
      hover: {
        bg: COLORS.actionButtonBackgroundHover,
      },
    },
  });
  actionButton.on('click', async () => {
    // console.log('actionButton click: ', {actionName});
    const nextState = await updateSimulation(`${index}`);
    STATE_EVENT_EMITTER.emit('update', nextState);
  });
  return actionButton;
};


// ////
// numbers views
// ////

const createLeftNumberItem = ({blessed, yNumber, index}) => {
  const boardIndex = index * BOARD_NUMBER_SPACING + 1;
  const leftNumberItem = blessed.box({
    right: 0,
    top: boardIndex * DIMENSIONS.tile.height + 1,
    content: yNumber,
    width: yNumber.length,
    height: 1,
    style: {
      fg: 'yellow',
      bg: COLORS.background,
    },
  });
  return leftNumberItem;
};

const generateLeftNumberItems = ({blessed, yNumbers}) => {
  const leftNumberItems = _.map(yNumbers, (yNumber, index) => {
    const leftNumberItem = createLeftNumberItem({blessed, yNumber, index});
    return leftNumberItem;
  });
  return leftNumberItems;
};

const createLeftNumbersContainer = ({blessed, initState}) => {
  const yNumbers = generateBoardNumbers(initState.bounds).y;
  const leftNumbersContainer = blessed.box({
    left: DIMENSIONS.titleBar.left - 1,
    top: DIMENSIONS.actionButton.height,
    width: DIMENSIONS.leftNumbersContainer.width(initState.bounds),
    height: DIMENSIONS.boardContainer.height(initState.bounds),
    style: {
      // bg: 'green',
      bg: COLORS.background,
    },
  });
  let leftNumberItems = generateLeftNumberItems({blessed, yNumbers}); // stateful!!
  _.each(leftNumberItems, (leftNumberItem) => {
    leftNumbersContainer.append(leftNumberItem);
  });
  let storedState = initState; // stateful!!
  leftNumbersContainer.refresh = (nextState) => {
    if (!_.isEqual(nextState.bounds, storedState.bounds)) {
      storedState = nextState;
      const yNumbers = generateBoardNumbers(nextState.bounds).y;
      _.each(leftNumberItems, (leftNumberItem) => {
        leftNumbersContainer.remove(leftNumberItem);
      });
      leftNumberItems = generateLeftNumberItems({blessed, yNumbers});
      _.each(leftNumberItems, (leftNumberItem) => {
        leftNumbersContainer.append(leftNumberItem);
      });
    }
  };
  return leftNumbersContainer;
};

const createTopNumberItem = ({blessed, xNumber, index}) => {
  const boardIndex = index * BOARD_NUMBER_SPACING + 1;
  const topNumberItem = blessed.box({
    left: boardIndex * DIMENSIONS.tile.width - xNumber.length + 3,
    top: 0,
    content: xNumber,
    width: xNumber.length,
    height: 1,
    style: {
      fg: 'yellow',
      bg: COLORS.background,
    },
  });
  return topNumberItem;
};

const generateTopNumberItems = ({blessed, xNumbers}) => {
  const topNumberItems = _.map(xNumbers, (xNumber, index) => {
    const topNumberItem = createTopNumberItem({blessed, xNumber, index});
    return topNumberItem;
  });
  return topNumberItems;
};

const createTopNumbersContainer = ({blessed, initState}) => {
  const xNumbers = generateBoardNumbers(initState.bounds).x;
  const topNumbersContainer = blessed.box({
    left: -1 + DIMENSIONS.titleBar.left + DIMENSIONS.leftNumbersContainer.width(initState.bounds),
    top: DIMENSIONS.actionButton.height - 1,
    width: DIMENSIONS.boardContainer.width(initState.bounds),
    height: 1,
    style: {
      // bg: 'green',
      bg: COLORS.background,
    },
  });
  let topNumberItems = generateTopNumberItems({blessed, xNumbers}); // stateful!!
  _.each(topNumberItems, (topNumberItem) => {
    topNumbersContainer.append(topNumberItem);
  });
  let storedState = initState; // stateful!!
  topNumbersContainer.refresh = (nextState) => {
    if (!_.isEqual(nextState.bounds, storedState.bounds)) {
      storedState = nextState;
      const xNumbers = generateBoardNumbers(nextState.bounds).x;
      _.each(topNumberItems, (topNumberItem) => {
        topNumbersContainer.remove(topNumberItem);
      });
      topNumberItems = generateTopNumberItems({blessed, xNumbers});
      _.each(topNumberItems, (topNumberItem) => {
        topNumbersContainer.append(topNumberItem);
      });
    }
  };
  return topNumbersContainer;
};

// ////
// board & subcomonents
// ////

const generateTileContents = (position, agent, board) => {
  let content = '';
  let color = '';
  if (_.isEqual(position, _.pick(agent, ['x', 'y']))) {
    switch (agent.orientation) {
      case ORIENTATIONS.north: {
        content = TILE_CHARS.north;
        break;
      }
      case ORIENTATIONS.east: {
        content = TILE_CHARS.east;
        break;
      }
      case ORIENTATIONS.south: {
        content = TILE_CHARS.south;
        break;
      }
      case ORIENTATIONS.west: {
        content = TILE_CHARS.west;
        break;
      }
      default: {
        throw new Error('DevErr: invalid orientation - should never happen..');
      }
    }
    if (!agent.isAlive) {
      color = TILE_COLORS.death;
      content = TILE_CHARS.death;
    } else if (agent.hasQuit) {
      color = TILE_COLORS.quit;
    } else {
      color = TILE_COLORS.alive;
    }
  } else if (_.find(board.tiles, position)) {
    content = TILE_CHARS.safe;
    color = TILE_COLORS.safe;
    if (_.find(agent.trajectory, position)) {
      color = TILE_COLORS.trodden;
    }
  } else {
    content = TILE_CHARS.bomb;
    color = TILE_COLORS.bomb;
  }
  return {content, color};
};

const createTile = ({blessed, state, indX, indY}) => {
  const position = {
    x: state.bounds.minX + indX - 1,
    y: state.bounds.minY + indY - 1,
  };
  const {color, content} = generateTileContents(position, state.agent, state.board);
  const tile = blessed.box({
    height: 1,
    width: 2,
    top: (position.y - state.bounds.minY + 1) * DIMENSIONS.tile.height + 1,
    left: (position.x - state.bounds.minX + 1) * DIMENSIONS.tile.width + 2,
    content: `${content}`,
    style: {
      fg: color,
      bg: COLORS.boardBackgroundEmpty,
    },
  });
  return tile;
};

const generateTiles = ({blessed, state}) => {
  const tiles = [];
  _.times(state.bounds.diffX + 3, (indX) => {
    _.times(state.bounds.diffY + 3, (indY) => {
      const tile = createTile({blessed, state, indX, indY});
      tiles.push(tile);
    });
  });
  return tiles;
};

const createBoardContainer = ({blessed, initState}) => {
  // board background
  const boardContainer = blessed.box({
    left: DIMENSIONS.leftNumbersContainer.width(initState.bounds) + 2,
    top: DIMENSIONS.actionButton.height,
    width: DIMENSIONS.boardContainer.width(initState.bounds),
    height: DIMENSIONS.boardContainer.height(initState.bounds),
    style: {
      bg: COLORS.boardBackgroundEmpty,
    },
  });
  //  tiles
  let tiles = generateTiles({blessed, state: initState}); // stateful!!
  _.each(tiles, (tile) => {
    boardContainer.append(tile);
  });
  boardContainer.refresh = (nextState) => {
    boardContainer.width = DIMENSIONS.boardContainer.width(nextState.bounds);
    boardContainer.height = DIMENSIONS.boardContainer.height(nextState.bounds);
    _.each(tiles, (tile) => {
      boardContainer.remove(tile);
    });
    tiles = generateTiles({blessed, state: nextState});
    _.each(tiles, (tile) => {
      boardContainer.append(tile);
    });
  };
  // done
  return boardContainer;
};

const generateStateString = ({agent}) => {
  const stateString = `  state = {
    x: ${agent.x},
    y: ${agent.y},
    orientation: "${agent.orientation}",
    numExecuted: ${agent.numExecuted},
    isAlive: ${agent.isAlive},
    hasQuit: ${agent.hasQuit},
  };`;
  return stateString;
};

// ////
// stateBox
// ////

const createStateBox = ({blessed, initState}) => {
  const stateString = generateStateString({agent: initState.agent});
  const stateBox = blessed.box({
    left: 2 + DIMENSIONS.leftNumbersContainer.width(initState.bounds),
    top: 1 + DIMENSIONS.actionButton.height + DIMENSIONS.boardContainer.height(initState.bounds),
    height: 8,
    width: 25,
    content: stateString,
    style: {
      fg: COLORS.stateBoxForeground,
      bg: COLORS.background,
    },
  });
  stateBox.refresh = (nextState) => {
    const nextStateString = generateStateString({agent: nextState.agent});
    stateBox.setContent(nextStateString);
    stateBox.top = 1 + DIMENSIONS.actionButton.height + DIMENSIONS.boardContainer.height(nextState.bounds);
  };
  return stateBox;
};


// ////
// createBoardView (main export)
// ////

const createBoardView = ({blessed, screen, state: initStateRaw}) => {
  // create the main view (i.e. a background)
  const background = createBackground({blessed});
  // then add the rest to the background
  const initState = {
    ...initStateRaw,
    bounds: getBoardBounds(initStateRaw.board),
  };
  _.each(ORDERED_COMMANDS, (command, index) => {
    const actionButton = createActionButton({blessed, command, index});
    background.append(actionButton);
  });
  const leftNumbersContainer = createLeftNumbersContainer({blessed, initState});
  background.append(leftNumbersContainer);
  const topNumbersContainer = createTopNumbersContainer({blessed, initState});
  background.append(topNumbersContainer);
  const boardContainer = createBoardContainer({blessed, initState});
  background.append(boardContainer);
  const stateBox = createStateBox({blessed, initState});
  background.append(stateBox);

  // add event handling
  const refreshBackgroundSubviews = (nextStateRaw) => {
    const nextState = {
      ...nextStateRaw,
      bounds: getBoardBounds(nextStateRaw.board),
    };
    const newBounds = getBoardBounds(nextState.board);
    topNumbersContainer.refresh({...nextState, bounds: newBounds});
    leftNumbersContainer.refresh({...nextState, bounds: newBounds});
    boardContainer.refresh({...nextState, bounds: newBounds});
    stateBox.refresh({...nextState, bounds: newBounds});
    screen.render();
  };
  STATE_EVENT_EMITTER.on('update', (nextState) => {
    refreshBackgroundSubviews(nextState);
  });
  background.refresh = refreshBackgroundSubviews;

  return background;
};

module.exports = {
  createBoardView,
};
