'use strict';

const {createBoardView} = require('./board-view');
const {createIntroView} = require('./intro-view');

module.exports = {
  createBoardView,
  createIntroView,
};
