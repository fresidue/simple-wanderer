'use strict';

const _ = require('lodash');
const delay = require('delay');

const {
  ORDERED_COMMANDS,
  COMMAND_INDICES,
  ORIENTATIONS,
  ORDERED_ORIENTATIONS,
} = require('../../constants');

const {
  runSimulation,
  updateSimulation,
  clearSimulation,
} = require('../../simulation');

const {
  validateManagedInput,
  parseInput,
} = require('../../input-parser');

const {
  TITLE,
  COLORS,
  TILE_CHARS,
  TILE_COLORS,
  ACTION_NAME_BY_COMMAND,
  // COMMAND_BY_ACTION_NAME,
  BOARD_NUMBER_SPACING,
  STATE_EVENT_EMITTER,
  STATE_EVENT_TYPES,
  MAIN_VIEW_TYPES,
} = require('../constants');

const {
  DIMENSIONS,
  generateBoardNumbers,
  getBoardBounds,
} = require('../utils');

const EventEmitter = require('events');

// ////
// constants
// ////

const INTRO_EVENT_EMITTER = new EventEmitter();
const INTRO_EVENT_TYPES = {
  editBegin: 'editBegin',
  editDimension: 'editDimension',
  openRemovals: 'openRemovals',
  closeRemovals: 'closeRemovals',
  setRemoval: 'setRemoval',
};

// ////
//
// ////

const INTRO_BUTTON_DIMS = {width: 9, height: 3};
const generateButtonProps = (color) => ({
  width: INTRO_BUTTON_DIMS.width,
  height: INTRO_BUTTON_DIMS.height,
  border: 'line',
  style: {
    bg: color,
    fg: 'white',
    border: {fg: color},
    focus: {
      border: {fg: 'white'},
    },
    hover: {
      border: {fg: 'white'},
    },
  },
});

const INPUT_LABEL_DIMS = {
  top: 1,
  width: 8,
  height: 1,
};

const TEXT_INPUT_DIMS = {
  top: 0,
  width: 12,
  height: 3,
};

const CHECKBOX_INPUT_DIMS = {
  top: 0,
  width: 6,
  height: 3,
};

const generateRightJustifiedString = ({value, width}) => {
  const padding = _.times(width - _.size(value), () => ' ').join('');
  const paddedValue = `${padding}${value}`;
  return paddedValue;
};

const generateLeftPreferredJustifiedString = ({value, width}) => {
  const valStr = String(value);
  const length = _.size(valStr);
  if (length < width) {
    return valStr;
  }
  const startIndex = length - width + 5;



  // const startIndex = Math.max(0, length - width);
  const apodizedString = `...${valStr.substring(startIndex)}`;
  // console.log('generateLeftPreferredJustifiedString', {value, width, apodizedString, startIndex});
  // console.log('XXXXXXX', {valStr, width, apodizedString, partial: valStr.substring(startIndex)});
  return apodizedString;
};

// ////
// main background (the blessed object that actually gets returned)
// ////

const createBackground = ({blessed}) => {
  const background = blessed.box({
    left: 0,
    top: DIMENSIONS.titleBar.height,
    width: '100%',
    height: '100%',
    style: {
      bg: COLORS.background,
    },
  });
  return background;
};

// ////
// sub-views
// ////

const createInputLine = ({blessed, state}) => {
  console.log('createInputLine');
  const content = !state.input ? '(no input)' : `RAW input: ${state.input}`;
  const inputLine = blessed.box({
    left: DIMENSIONS.titleBar.left + 2,
    top: 1,
    width: '100%',
    height: DIMENSIONS.inputLine.height,
    content,
    style: {
      bg: COLORS.background,
      fg: 'white',
    },
  });
  return inputLine;
};

const createParsedInputBox = ({blessed, state}) => {
  const PAD = _.times(DIMENSIONS.titleBar.left, () => ' ').join('');
  console.log('createParsedInputBox');
  const content = `
${PAD}parsedInput: {
${PAD}  boardSize: {width: ${state.parsedInput.dimensions.width}, height: ${state.parsedInput.dimensions.height}},
${PAD}  position: {x: ${state.parsedInput.position.x}, y: ${state.parsedInput.position.y}},
${PAD}  commands: [${_.map(state.parsedInput.commands, command => COMMAND_INDICES[command]).join(', ')}],
${PAD}}
  `;
  const parsedInputBox = blessed.box({
    left: -1 + DIMENSIONS.titleBar.left,
    top: 1 + DIMENSIONS.inputLine.height,
    width: '100%',
    height: DIMENSIONS.parsedInputBox.height,
    content,
    style: {
      bg: COLORS.background,
      fg: 'green',
    },
  });
  return parsedInputBox;
};

const createManagedInputBox = ({blessed, state: initState}) => {
  // console.log({initStateManagedInput: initState.managedInput});
  // console.log('orientation is string = ', _.isString(initState.managedInput.orientation));
  const PAD = _.times(DIMENSIONS.titleBar.left, () => ' ').join('');
  const generateContent = (state) => `
${PAD}managedInput: {
${PAD}  boardSize: {width: ${state.managedInput?.dimensions.width}, height: ${state.managedInput.dimensions.height}},
${PAD}  position: {x: ${state.managedInput.position.x}, y: ${state.managedInput.position.y}},
${PAD}  orientation: ${state.managedInput.orientation},
${PAD}  commands: [${_.map(state.managedInput.commands, command => COMMAND_INDICES[command]).join(', ')}],
${PAD}  removals: [${_.map(state.managedInput.removals, removal => `{x: ${removal.x}, y: ${removal.y}}`).join(',  ')}],
${PAD}}
  `;

  const managedInputBox = blessed.box({
    left: -1 + DIMENSIONS.titleBar.left,
    top: 1 + DIMENSIONS.inputLine.height + (initState.parsedInput ? DIMENSIONS.parsedInputBox.height : DIMENSIONS.parseErrorBox.height),
    width: '100%',
    height: DIMENSIONS.managedInputBox.height,
    content: generateContent(initState),
    style: {
      bg: COLORS.background,
      fg: 'white',
    },
  });
  managedInputBox.refresh = (nextState) => {
    // console.log('refresh nextStateManagedInput = ', nextState.managedInput);
    const nextContent = generateContent(nextState);
    managedInputBox.setContent(nextContent);
  };
  return managedInputBox;
};

const createParseErrorBox = ({blessed, state}) => {
  const parseErrorBox = blessed.box({
    left: 2 + DIMENSIONS.titleBar.left,
    top: 2 + DIMENSIONS.inputLine.height,
    width: '100%',
    height: DIMENSIONS.parseErrorBox.height,
    content: `PARSE ERROR: ${state.parseError.message}`,
    style: {
      bg: COLORS.background,
      fg: 'red',
    },
  });
  return parseErrorBox;
};

const createIntroButtons = ({blessed, state}) => {

  const introButtonsContainer = blessed.box({
    left: 2 + DIMENSIONS.titleBar.left,
    top: 3 + DIMENSIONS.inputLine.height + DIMENSIONS.parsedInputBox.height,
    width: '100%',
    height: 3,
    style: {
      bg: COLORS.background,
    },
  });

  const runButton = blessed.button({
    left: 0,
    top: 0,
    content: '  run',
    ...generateButtonProps('green'),
  });
  introButtonsContainer.append(runButton);

  const editButton = blessed.button({
    left: 2 + INTRO_BUTTON_DIMS.width,
    top: 0,
    content: '  edit',
    ...generateButtonProps('yellow'),
  });
  introButtonsContainer.append(editButton);

  runButton.focus();
  const beginRunningSimulation = () => {
    let simState;
    try {
      simState = runSimulation(state.input);
    } catch (err) {
      throw new Error(`DevErr - valid input "${input}" should not break sim: ${err.message}`);
    }
    STATE_EVENT_EMITTER.emit(STATE_EVENT_TYPES.view, {viewType: MAIN_VIEW_TYPES.board, simState});
  };
  runButton.on('click', beginRunningSimulation);
  runButton.key(['enter'], beginRunningSimulation);
  runButton.key(['tab'], () => {
    editButton.focus();
  });
  editButton.on('click', () => {
    INTRO_EVENT_EMITTER.emit(INTRO_EVENT_TYPES.editBegin);
  })
  editButton.key(['enter'], () => {
    INTRO_EVENT_EMITTER.emit(INTRO_EVENT_TYPES.editBegin);
  });
  editButton.key(['tab'], () => {
    runButton.focus();
  });

  return introButtonsContainer;
};

// ////
// edit menu
// ////


const createInputRowHeader = ({blessed, top = 1, title}) => blessed.box({
  left: 0,
  top,
  width: '100%',
  height: 1,
  content: title,
  style: {
    bold: true,
    fg: 'yellow',
    bg: COLORS.background,
  },
});

const createInputRowContainer = ({blessed, top = 2}) => blessed.box({
  left: 0,
  top,
  width: '100%',
  height: 3,
  style: {
    // bg: 'red',
    bg: COLORS.background,
  },
});


const createInputLabel = ({blessed, left = 0, width = INPUT_LABEL_DIMS.width, value = ''}) => {
  const inputLabel = blessed.box({
    ...INPUT_LABEL_DIMS,
    left,
    width,
    style: {
      fg: 'white',
      bg: COLORS.background,
    },
    content: generateRightJustifiedString({value, width}),
  });
  return inputLabel;
};

const CHECKBOX_CHAR = '❖';
const CURSOR_CHAR = '☰';
const NEGATIVE_CHAR = '-'
const DIGIT_CHARS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

const createTextInput = ({blessed, screen, left = 0, width = TEXT_INPUT_DIMS.width, initValue, isStrictPositive, eventType, eventKey, enforcedOff}) => {
  let storedValue = `${initValue || ''}`; // stateful!!
  const textInput = blessed.box({
    ...TEXT_INPUT_DIMS,
    left,
    width,
    border: 'line',
    style: {
      bg: 'black',
      fg: 'white',
      border: {fg: 'white'},
      focus: {
        border: {fg: 'green'},
      },
      hover: {
        border: {fg: 'yellow'},
      },
    },
    content: generateLeftPreferredJustifiedString({value: storedValue, width}),
  });
  // event-handling
  let isFocused = false;
  const DELAY_PERIOD = {on: 400, off: 400}; // ms
  textInput.on('focus', async () => {
    isFocused = true;
    const oscillate = async (wasOn) => {
      if (!isFocused) {
        return; // lost focus in delay
      }
      const nextChar = wasOn ? ' ' : CURSOR_CHAR;
      textInput.setContent(generateLeftPreferredJustifiedString({value: `${storedValue}${nextChar}`, width}));
      screen.render();
      await delay(wasOn ? DELAY_PERIOD.off : DELAY_PERIOD.on);
      await oscillate(!wasOn);
    };
    try {
      await oscillate(false);
    } catch (err) {
      throw new Error('DevErr: oscillations ought never fail..');
    }
  });
  textInput.on('blur', () => {
    isFocused = false;
    textInput.setContent(generateLeftPreferredJustifiedString({value: storedValue, width}));
    screen.render();
  });
  textInput.key(['backspace'], () => {
    if (storedValue.length) {
      storedValue = storedValue.substring(0, storedValue.length - 1);
      // console.log('backspace emitting', eventType, {key: eventKey, value: storedValue});
      if (!storedValue.length || isNaN(Number(storedValue))) {
        INTRO_EVENT_EMITTER.emit(eventType, {key: eventKey, value: null});
      } else {
        INTRO_EVENT_EMITTER.emit(eventType, {key: eventKey, value: Number(storedValue)});
      }
      _.each(enforcedOff, enforcedView => enforcedView.forceOff());
    }
  });
  textInput.key(_.concat(DIGIT_CHARS, NEGATIVE_CHAR), (ch) => {
    if (ch === NEGATIVE_CHAR && !isStrictPositive && !storedValue.length) {
      storedValue = NEGATIVE_CHAR;
    }
    if (_.includes(DIGIT_CHARS, ch)) {
      storedValue = `${storedValue}${ch}`;
    }
    if (!storedValue.length || isNaN(Number(storedValue))) {
      INTRO_EVENT_EMITTER.emit(eventType, {key: eventKey, value: null});
    } else {
      INTRO_EVENT_EMITTER.emit(eventType, {key: eventKey, value: Number(storedValue)});
    }
    _.each(enforcedOff, enforcedView => enforcedView.forceOff());
  });
  return textInput;
};

const createCheckboxInput = ({blessed, left = 0, isChecked = false}) => {
  let storedIsChecked = !!isChecked;
  const generateCheckboxContent = () => {
    const content = ` ${storedIsChecked ? CHECKBOX_CHAR : ' '} `;
    return content;
  };
  const checkboxInput = blessed.box({
    ...CHECKBOX_INPUT_DIMS,
    left,
    border: 'line',
    style: {
      bg: 'black',
      fg: 'white',
      border: {fg: 'white'},
      focus: {
        fg: 'green',
        border: {fg: 'green'},
      },
      hover: {
        border: {fg: 'yellow'},
      },
    },
    content: generateCheckboxContent(),
  });
  const toggleCheckboxInput = () => {
    storedIsChecked = !storedIsChecked;
    checkboxInput.setContent(generateCheckboxContent());
    INTRO_EVENT_EMITTER.emit(storedIsChecked ? INTRO_EVENT_TYPES.openRemovals : INTRO_EVENT_TYPES.closeRemovals);
  };
  checkboxInput.on('click', () => {
    toggleCheckboxInput();

  });
  checkboxInput.key(['enter'], () => {
    toggleCheckboxInput();
  });
  checkboxInput.forceOff = () => {
    storedIsChecked = false;
    checkboxInput.setContent(generateCheckboxContent());
  };
  return checkboxInput;
};

const createEditMenu = ({blessed, screen, state}) => {

  const editMenuContainer = blessed.box({
    left: 2 + DIMENSIONS.titleBar.left,
    top: 2 + DIMENSIONS.inputLine.height + (state.parsedInput ? DIMENSIONS.parsedInputBox.height : DIMENSIONS.parseErrorBox.height) + DIMENSIONS.managedInputBox.height,
    width: '100%',
    height: '100%',
    style: {
      // bg: 'blue',
      bg: COLORS.background,
    },
  });

  // // // // from the bottom - needed for forced removals
  const REMOVAL_LABEL_WIDTH = 15;
  const editRemovalsInput = createCheckboxInput({blessed, left: 1 + REMOVAL_LABEL_WIDTH, isChecked: false});

  // // // // row 1
  const editDimensionsHeader = createInputRowHeader({blessed, top: 1, title: 'SET BOARD DIMENSIONS:'});
  editMenuContainer.append(editDimensionsHeader);
  const editDimensionsRow = createInputRowContainer({blessed, top: 2});
  editMenuContainer.append(editDimensionsRow);

  // width
  const widthLabel = createInputLabel({blessed, left: 0, value: 'width:'});
  editDimensionsRow.append(widthLabel);
  const initWidthValue = _.get(state, 'parsedInput.dimensions.width');
  const widthInputLeft = 1 + INPUT_LABEL_DIMS.width;
  const widthInput = createTextInput({
    blessed,
    screen,
    left: widthInputLeft,
    initValue: initWidthValue,
    isStrictPositive: true,
    eventType: INTRO_EVENT_TYPES.editDimension,
    eventKey: 'width',
    enforcedOff: [editRemovalsInput],
  });
  editDimensionsRow.append(widthInput);

  // height
  const heightLabelLeft = 1 + INPUT_LABEL_DIMS.width + 2 + TEXT_INPUT_DIMS.width;
  const heightLabel = createInputLabel({blessed, left: heightLabelLeft, value: 'height:'});
  editDimensionsRow.append(heightLabel);
  const initHeightValue = _.get(state, 'parsedInput.dimensions.height');
  const heightInputLeft = 2 * (1 + INPUT_LABEL_DIMS.width) + 2 + TEXT_INPUT_DIMS.width;
  const heightInput = createTextInput({
    blessed,
    screen,
    left: heightInputLeft,
    initValue: initHeightValue,
    isStrictPositive: true,
    eventType: INTRO_EVENT_TYPES.editDimension,
    eventKey: 'height',
    enforcedOff: [editRemovalsInput],
  });
  editDimensionsRow.append(heightInput);

  // // // // row 2

  const editPositionHeader = createInputRowHeader({blessed, top: 6, title: 'SET AGENT POSITION:'});
  editMenuContainer.append(editPositionHeader);
  const editPositionRow = createInputRowContainer({blessed, top: 7});
  editMenuContainer.append(editPositionRow);

  // width
  const posXLabel = createInputLabel({blessed, left: 0, value: 'pos.x:'});
  editPositionRow.append(posXLabel);
  const initPosXValue = _.get(state, 'parsedInput.position.x');
  const posXInputLeft = 1 + INPUT_LABEL_DIMS.width;
  const posXInput = createTextInput({
    blessed,
    screen,
    left: posXInputLeft,
    initValue: initPosXValue,
    eventType: INTRO_EVENT_TYPES.editPosition,
    eventKey: 'x',
  });
  editPositionRow.append(posXInput);

  // height
  const posYLabelLeft = 1 + INPUT_LABEL_DIMS.width + 2 + TEXT_INPUT_DIMS.width;
  const posYLabel = createInputLabel({blessed, left: posYLabelLeft, value: 'pos.y:'});
  editPositionRow.append(posYLabel);
  const initPosYValue = _.get(state, 'parsedInput.position.y');
  const posYInputLeft = 2 * (1 + INPUT_LABEL_DIMS.width) + 2 + TEXT_INPUT_DIMS.width;
  const posYInput = createTextInput({
    blessed,
    screen,
    left: posYInputLeft,
    initValue: initPosYValue,
    eventType: INTRO_EVENT_TYPES.editPosition,
    eventKey: 'y',
  });
  editPositionRow.append(posYInput);

  // // // // row 3
  const editOrientationHeader = createInputRowHeader({blessed, top: 11, title: 'SET AGENT ORIENTATION:'});
  editMenuContainer.append(editOrientationHeader);
  const editOrientationRow = createInputRowContainer({blessed, top: 12});
  editMenuContainer.append(editOrientationRow);
  const createOrientationButton = ({blessed, screen, index, orientation, state: initState}) => {
    let isSelected = initState.managedInput.orientation === orientation; // local state
    const getFgColor = () => isSelected ? 'green' : 'white';
    const orientationButton = blessed.box({
      top: 0,
      left: 2 + index * 11,
      width: 9,
      height: 3,
      border: 'line',
      style: {
        bg: COLORS.background,
        // bg: 'black',
        fg: getFgColor(),
        border: {fg: 'white'},
        focus: {
          border: {fg: 'green'},
        },
        hover: {
          border: {fg: 'green'},
        },
      },
      content: ` ${orientation}`,
    });
    orientationButton.refresh = (nextOrientation) => {
      isSelected = nextOrientation === orientation;
      // orientationButton.style.fg = getFgColor();
      // console.log('setting color ');
      orientationButton.style = {...orientationButton.style, fg: getFgColor()};
    };
    orientationButton.isSelected = () => isSelected;
    return orientationButton;
  };
  const orientationButtons = [];
  _.each(ORDERED_ORIENTATIONS, (orientation, index) => {
    const orientationButton = createOrientationButton({blessed, screen, index, orientation, state});
    orientationButtons.push(orientationButton);
    editOrientationRow.append(orientationButton);
  });
  const refreshOrientationButtons = (nextOrientation) => {
    _.each(orientationButtons, orientationButton => {
      orientationButton.refresh(nextOrientation);
    });
  };false
  _.each(orientationButtons, (orientationButton, index) => {
    const buttonOrientation = ORDERED_ORIENTATIONS[index];
    orientationButton.on('click', () => {
      refreshOrientationButtons(buttonOrientation);
      INTRO_EVENT_EMITTER.emit(INTRO_EVENT_TYPES.orientation, buttonOrientation);
    });
    orientationButton.key('enter', () => {
      if (orientationButton.isSelected()) {
        const nextButtonIndex = (index + 1) % 4;
        const nextButtonOrientation = ORDERED_ORIENTATIONS[nextButtonIndex];
        refreshOrientationButtons(nextButtonOrientation);
        INTRO_EVENT_EMITTER.emit(INTRO_EVENT_TYPES.orientation, nextButtonOrientation);
        orientationButtons[nextButtonIndex].focus();
      } else {
        refreshOrientationButtons(buttonOrientation);
        INTRO_EVENT_EMITTER.emit(INTRO_EVENT_TYPES.orientation, buttonOrientation);
      }
    });
  });

  // // // // row 4

  const editRemovalsHeader = createInputRowHeader({blessed, top: 16, title: 'EDIT BOARD TILE REMOVALS:'});
  editMenuContainer.append(editRemovalsHeader);
  const editRemovalsRow = createInputRowContainer({blessed, top: 17});
  editMenuContainer.append(editRemovalsRow);

  // removals
  const removalsLabelLeft = 1 + INPUT_LABEL_DIMS.width;
  const removalsLabel = createInputLabel({blessed, left: 0, width: REMOVAL_LABEL_WIDTH, value: 'board.removals:'});
  editRemovalsRow.append(removalsLabel);
  editRemovalsRow.append(editRemovalsInput);

  // generate the focus chain
  const CORE_FOCUS_INPUTS = [
    widthInput,
    heightInput,
    posXInput,
    posYInput,
    ...orientationButtons,
    editRemovalsInput,
  ];
  const focusChain = [];

  const getFocusChainNeighbor = (index, isNext) => {
    const diff = isNext ? 1 : -1;
    const neighborIndex = (index + diff + focusChain.length) % focusChain.length;
    return focusChain[neighborIndex];
  };
  const appendToFocusChain = (focusableInput) => {
    let index = focusChain.length;
    focusableInput.key(['tab'], () => {
      const nextNeighbor = getFocusChainNeighbor(index, true);
      nextNeighbor.focus();
    });
    focusableInput.key(['S-tab'], () => {
      const prevNeighbor = getFocusChainNeighbor(index, false);
      prevNeighbor.focus();
    });
    focusChain.push(focusableInput);
    const reappend = () => {
      index = focusChain.length;
      focusChain.push(focusableInput);
    };
    return reappend;
  };
  _.each(CORE_FOCUS_INPUTS, appendToFocusChain);
  focusChain[0].focus();

  // package up -- the focusChain should really not be rooted in here I guess..
  editMenuContainer.focusChain = focusChain;
  editMenuContainer.appendToFocusChain = appendToFocusChain;
  return editMenuContainer;
};

// ////
// editRemovalsBox
// ////

const createRemovalButton = ({blessed, xIndex, yIndex, isRemoval: initIsRemoval}) => {
  let storedIsRemoval = !!initIsRemoval; // mutable!!
  const generateContent = (isR) => {
    const content = isR ? `\n  ${TILE_CHARS.bomb}` : '';
    return content;
  };
  const removalButton = blessed.box({
    left: 1 + xIndex * DIMENSIONS.removalBoardButton.width,
    top: 1 + yIndex * DIMENSIONS.removalBoardButton.height,
    width: DIMENSIONS.removalBoardButton.width,
    height: DIMENSIONS.removalBoardButton.height,
    style: {
      bg: COLORS.background,
      fg: 'blue',
      focus: {
        bg: COLORS.actionButtonBackgroundHover,
      },
      hover: {
        bg: COLORS.actionButtonBackgroundHover,
      },
    },
    content: generateContent(initIsRemoval),
  });
  const toggle = () => {
    storedIsRemoval = !storedIsRemoval;
    const nextContent = generateContent(storedIsRemoval);
    removalButton.setContent(nextContent);
    INTRO_EVENT_EMITTER.emit(INTRO_EVENT_TYPES.setRemoval, {x: xIndex, y: yIndex, isRemoval: storedIsRemoval});
  };
  removalButton.on('click', () => {
    removalButton.focus();
    toggle();
  });
  removalButton.key(['enter'], () => {
    toggle();
  });
  return removalButton;
};

const createEditRemovalsBox = ({blessed, screen, state, appendToFocusChain}) => {
  const width = _.get(state, 'managedInput.dimensions.width');
  const height = _.get(state, 'managedInput.dimensions.height');
  const hasDimensions =  width && height;
  // const hasDimensions = state.dimensions.width && state.dimensions.height;
  const editRemovalsBox = blessed.box({
    left: 2 + DIMENSIONS.titleBar.left,
    top: 2 + DIMENSIONS.editRemovalsBox.top({parsedInput: state.parsedInput}),

    // top: 2 + DIMENSIONS.inputLine.height + (state.parsedInput ? DIMENSIONS.parsedInputBox.height : DIMENSIONS.parseErrorBox.height) + DIMENSIONS.managedInputBox.height + DIMENSIONS.editMenu.height,
    width: '100%',
    height: DIMENSIONS.editRemovalsBox.height({width, height}),
    // height: !hasDimensions ? 1 : DIMENSIONS.removalBoardButton.height * height,

    style: {
      bg: COLORS.background,
    },
  });

  if (!hasDimensions) {
    // early return
    const invalidDimensionsBox = blessed.box({
      left: 1,
      top: 0,
      width: '100%',
      height: 1,
      style: {
        bg: COLORS.background,
        fg: 'red',
      },
      content: `INVALID DIMENSIONS: {dimensions: {width: ${width}, height: ${height}}}`,
    });
    editRemovalsBox.append(invalidDimensionsBox);
  }

  if (hasDimensions) {
    const removalButtonsContainer = blessed.box({
      left: 0,
      top: 0,
      width: 2 + DIMENSIONS.removalBoardButton.width * width,
      height: 2 + DIMENSIONS.removalBoardButton.height * height,
      border: 'line',
      style: {
        border: {fg: 'blue'},
      },
    });
    editRemovalsBox.append(removalButtonsContainer);
    let removalButtons = [];
    _.times(height, yIndex => {
      _.times(width, xIndex => {
        const isRemoval = !!_.find();
        const removalButton = createRemovalButton({blessed, screen, xIndex, yIndex, isRemoval});
        removalButtons.push(removalButton);
        editRemovalsBox.append(removalButton);
        appendToFocusChain(removalButton);
      });
    });
  }

  return editRemovalsBox;
};

const createFooter = ({blessed, screen, state: initState}) => {

  let storedState = _.clone(initState); // for button handling

  const calcTop = (state) => {
    const basicTop = 2 + DIMENSIONS.editRemovalsBox.top({parsedInput: state.parsedInput});
    const extraTop = !state.isEditingRemovals ? 0 : 3 + DIMENSIONS.editRemovalsBox.height({
      width: _.get(state, 'managedInput.dimensions.width'),
      height: _.get(state, 'managedInput.dimensions.height'),
    });
    return basicTop + extraTop;
  };

  const footer = blessed.box({
    top: calcTop(initState),
    left: 0,
    width: '100%',
    height: '100%',
    style: {
      bg: COLORS.background,
    },
  });

  const VALID_STYLE = {
    bg: COLORS.background,
    fg: 'green',
    border: {fg: 'white'},
    focus: {
      border: {fg: 'green'},
    },
    hover: {
      border: {fg: 'green'},
    },
  };
  const INVALID_STYLE = {
    bg: COLORS.background,
    fg: 'red',
    border: {fg: 'white'},
    focus: {
      border: {fg: 'red'},
    },
    hover: {
      border: {fg: 'red'},
    },
  };


  const finishButton = blessed.box({
    left: 2 + DIMENSIONS.titleBar.left,
    top: 0,
    width: 20,
    height: 3,
    border: 'line',
    style: initState.managedInputError ? INVALID_STYLE : VALID_STYLE,
    content: initState.managedInputError ? ' invalid inputs' : '  run simulation',
  });
  finishButton.on('click', () => {
    if (!storedState.managedInputError) {
      let simState;
      try {
        // console.log('managedInput: ', storedState.managedInput);
        simState = runSimulation(storedState.managedInput);
        STATE_EVENT_EMITTER.emit(STATE_EVENT_TYPES.view, {viewType: MAIN_VIEW_TYPES.board, simState});
      } catch (err) {
        throw new Error(`DevErr - valid managedInput "${storedState.managedInput}" should not break sim: ${err.message}`);
      }
    }
  });
  footer.append(finishButton);

  const errorBox = blessed.box({
    left: 2 + DIMENSIONS.titleBar.left,
    top: 4,
    width: `100%-${2 + DIMENSIONS.titleBar.left}`,
    height: 3,
    style: {
      bg: COLORS.background,
      fg: 'red',
    },
    content: initState.managedInputError ? initState.managedInputError.message : '',
  });
  footer.append(errorBox);

  footer.refresh = (nextState) => {
    footer.top = calcTop(nextState);
    finishButton.style = nextState.managedInputError ? INVALID_STYLE : VALID_STYLE; // not sure why border color not changing....
    finishButton.setContent(nextState.managedInputError ? ' invalid inputs' : '  run simulation');
    errorBox.setContent(nextState.managedInputError ? nextState.managedInputError.message : '');
    storedState = _.clone(nextState);
  };
  footer.finishButton = finishButton;
  return footer;
};

// ////
// introView
// ////

// mutable
const state = {
  input: null,
  parsedInput: null,
  parseError: null,
  isEditing: false,
  isEditingRemovals: false,
  managedInput: null,
  managedInputError: null,
};
const updateManagedInputError = () => {
  try {
    validateManagedInput(state.managedInput);
    state.managedInputError = null;
  } catch (err) {
    state.managedInputError = err;
  }
};
const DEFAULT_MANAGED_INPUT = {
  dimensions: {width: null, height: null},
  position: {x: null, y: null},
  commands: [],
  removals: [],
  orientation: ORIENTATIONS.north,
};

const createIntroView = ({blessed, screen, input}) => {

  state.input = input;
  try {
    const usesHeader = true;
    state.parsedInput = parseInput(input, usesHeader);
    state.managedInput = _.assign(
      _.clone(DEFAULT_MANAGED_INPUT),
      _.clone(state.parsedInput),
    );
    updateManagedInputError();
  } catch (err) {
    state.parseError = err;
    state.managedInput = _.clone(DEFAULT_MANAGED_INPUT);
    updateManagedInputError();
  }

  // create the main view (i.e. a background)
  const background = createBackground({blessed});

  // static content
  const inputLine = createInputLine({blessed, state});
  background.append(inputLine);
  if (state.parsedInput) {
    const parsedInputBox = createParsedInputBox({blessed, state});
    background.append(parsedInputBox);
  }
  if (state.parseError) {
    const parseErrorBox = createParseErrorBox({blessed, state});
    background.append(parseErrorBox);
  }

  let introButtons = null;
  let editMenu = null;
  let managedInputBox = null;
  let footer = null;
  let reappendFooterToFocusChain;
  const addEditMenu = () => {
    if (introButtons) {
      background.remove(introButtons);
      introButtons = null;
    }
    state.isEditing = true;
    managedInputBox = createManagedInputBox({blessed, state});
    background.append(managedInputBox);
    editMenu = createEditMenu({blessed, screen, state});
    background.append(editMenu);
    footer = createFooter({blessed, screen, state});
    background.append(footer);
    reappendFooterToFocusChain = editMenu.appendToFocusChain(footer.finishButton);
  };
  if (state.parsedInput && !state.isEditing) {
    introButtons = createIntroButtons({blessed, state});
    background.append(introButtons);
  } else {
    addEditMenu();
  }

  INTRO_EVENT_EMITTER.on(INTRO_EVENT_TYPES.editBegin, () => {
    addEditMenu();
    screen.render();
  });

  INTRO_EVENT_EMITTER.on(INTRO_EVENT_TYPES.editDimension, ({key, value}) => {
    _.set(state, ['managedInput', 'dimensions', key], value);
    updateManagedInputError();
    managedInputBox.refresh(state);
    footer.refresh(state);
    INTRO_EVENT_EMITTER.emit(INTRO_EVENT_TYPES.closeRemovals); // renders
  });

  INTRO_EVENT_EMITTER.on(INTRO_EVENT_TYPES.editPosition, ({key, value}) => {
    _.set(state, ['managedInput', 'position', key], value);
    updateManagedInputError();
    managedInputBox.refresh(state);
    footer.refresh(state);
    screen.render();
  });

  INTRO_EVENT_EMITTER.on(INTRO_EVENT_TYPES.editOrientation, (orientation) => {
    state.managedInput.orientation = orientation;
    updateManagedInputError();
    managedInputBox.refresh(state);
    footer.refresh(state);
    screen.render();
  });

  let editRemovalsBox = null;
  INTRO_EVENT_EMITTER.on(INTRO_EVENT_TYPES.openRemovals, () => {
    state.isEditingRemovals = true;
    editMenu.focusChain.pop();
    editRemovalsBox = createEditRemovalsBox({blessed, screen, state, appendToFocusChain: editMenu.appendToFocusChain});
    reappendFooterToFocusChain();
    background.append(editRemovalsBox);
    footer.refresh(state);
    screen.render();
  });

  INTRO_EVENT_EMITTER.on(INTRO_EVENT_TYPES.closeRemovals, () => {
    state.isEditingRemovals = false;
    if (editRemovalsBox) {
      background.remove(editRemovalsBox);
      editRemovalsBox = null;
    }
    footer.refresh(state);
    const NUM_CORE_BUTTONS = 9;
    editMenu.focusChain.splice(NUM_CORE_BUTTONS);
    reappendFooterToFocusChain();
    screen.render();
  });

  INTRO_EVENT_EMITTER.on(INTRO_EVENT_TYPES.setRemoval, ({x, y, isRemoval}) => {
    if (isRemoval) {
      state.managedInput.removals.push({x, y});
    } else {
      state.managedInput.removals = _.filter(state.managedInput.removals, (removal) => {
        const isRemovedRemoval = x === removal.x && y === removal.y;
        return !isRemovedRemoval;
      });
    }
    updateManagedInputError();
    managedInputBox.refresh(state);
    footer.refresh(state);
    screen.render();
  });

  return background;
};

module.exports = {
  createIntroView,
};
