'use strict';

const EventEmitter = require('events');
const {processYargs, combineYargs} = require('./yargs-util');
const {runSimulation, updateSimulation} = require('./simulation');
const {initializeInteractive} = require('./interactive');

const stopSimulationEmitter = new EventEmitter();

const listen = async () => {
  // listen for data
  let simulationWasStopped = false;
  const yargs = await new Promise((resolve) => {
    process.stdin.on('data', (data) => {
      process.stdin.removeAllListeners('data');
      stopSimulationEmitter.removeAllListeners('stop');
      const lines = data.toString().split('\n');
      resolve(lines);
    });
    stopSimulationEmitter.on('stop', () => {
      process.stdin.removeAllListeners('data');
      stopSimulationEmitter.removeAllListeners('stop');
      simulationWasStopped = true;
      stopSimulationEmitter.emit('stopped');
      resolve();
    });
  });
  // end recursion from within if simulationWasStopped
  if (simulationWasStopped) {
    return null;
  }
  // otherwise process yargs
  const input = combineYargs(yargs);
  const {agent} = updateSimulation(input);
  if (!agent.isAlive) {
    return [-1, -1];
  }
  if (agent.hasQuit) {
    return [agent.x, agent.y];
  }
  // and recurse
  const finalRes = await listen();
  return finalRes;
};

const start = async (input) => {
  const {agent} = runSimulation(input);
  if (!agent.isAlive) {
    return [-1, -1];
  }
  if (agent.hasQuit) {
    return [agent.x, agent.y];
  }
  const finalRes = await listen();
  return finalRes;
};

const stop = async () => {
  await new Promise((resolve) => {
    stopSimulationEmitter.on('stopped', () => {
      process.stdin.removeAllListeners('data');
      stopSimulationEmitter.removeAllListeners('stop');
      stopSimulationEmitter.removeAllListeners('stopped');
      resolve();
    });
    stopSimulationEmitter.emit('stop');
  });
};

const startInteractive = async (input) => {
  const res = initializeInteractive(input);
  return res;
};


module.exports = {
  processYargs,
  start,
  stop,
  startInteractive,
};
