'use strict';

const ORIENTATIONS = {
  north: 'north',
  east: 'east',
  south: 'south',
  west: 'west',
};

const ORDERED_ORIENTATIONS = [
  ORIENTATIONS.north,
  ORIENTATIONS.east,
  ORIENTATIONS.south,
  ORIENTATIONS.west,
];

const ORIENTATION_INDICES = {
  [ORIENTATIONS.north]: 0,
  [ORIENTATIONS.east]: 1,
  [ORIENTATIONS.south]: 2,
  [ORIENTATIONS.west]: 3,
};

const DEFAULT_ORIENTATION = ORIENTATIONS.north;


// 0 = quit simulation and print results to stdout
// 1 = move forward one step
// 2 = move backwards one step
// 3 = rotate clockwise 90 degrees (eg north to east)
// 4 = rotate counterclockwise 90 degrees (eg west to south)
const COMMANDS = {
  quit: 'quit',
  moveForward: 'moveForward',
  moveBackward: 'moveBackward',
  rotateCW: 'rotateCW',
  rotateCCW: 'rotateCCW',
  boardRotateCW: 'boardRotateCW', // from agent-perspective, indistinguishable from rotateCCW
  boardRotateCCW: 'boardRotateCCW', // from agent-perspective, indistinguishable from rotateCW
};

const ORDERED_COMMANDS = [
  COMMANDS.quit,
  COMMANDS.moveForward,
  COMMANDS.moveBackward,
  COMMANDS.rotateCW,
  COMMANDS.rotateCCW,
  COMMANDS.boardRotateCW,
  COMMANDS.boardRotateCCW,
];

const COMMAND_INDICES = {
  [COMMANDS.quit]: 0,
  [COMMANDS.moveForward]: 1,
  [COMMANDS.moveBackward]: 2,
  [COMMANDS.rotateCW]: 3,
  [COMMANDS.rotateCCW]: 4,
  [COMMANDS.boardRotateCW]: 5,
  [COMMANDS.boardRotateCCW]: 6,
};


module.exports = {
  ORIENTATIONS,
  ORDERED_ORIENTATIONS,
  ORIENTATION_INDICES,
  DEFAULT_ORIENTATION,
  COMMANDS,
  ORDERED_COMMANDS,
  COMMAND_INDICES,
};
