'use strict';

const _ = require('lodash');
const {PropTypes, validateProps} = require('vanilla-prop-types');

const {
  ORIENTATIONS,
  ORIENTATION_INDICES,
  COMMAND_INDICES,
  ORDERED_COMMANDS,
} = require('./constants');

// ////
// validation
// ////

const validateRawInput = validateProps({
  inputRaw: PropTypes.string
    .isRequired
    .custom(({prop}) => !!prop)
    .error(new Error('the input must be a non-empty string')),
});

const validateInputList = validateProps({
  inputList: PropTypes.arrayOf(
    PropTypes.string.isRequired.custom(({prop, propName}) => {
      const val = Number(prop);
      if (isNaN(val) || Math.round(val) !== val) {
        throw new Error(`Invalid inputList item "${prop}" at index ${propName} (must be an integer)`);
      }
    }),
  ).isRequired,
});

const validateHeaderValues = validateProps({
  headerValues: PropTypes.arrayOf(
    PropTypes.number.isRequired.custom(({prop, propName}) => {
      // width & height are regulated (position is not restricted beyond being an integer)
      if (propName === 0 && prop < 1) {
        throw new Error(`Invalid table width ${prop} which must be greater than 0`);
      }
      if (propName === 1 && prop < 1) {
        throw new Error(`Invalid table height ${prop} which must be greater than 0`);
      }
    }),
  )
    .isRequired
    .custom(({prop}) => {
      if (prop.length !== 4) {
        throw new Error(`Invalid header values - there must be exactly 4, but ${prop.length} were found`);
      }
    }),
});

const validateCommandValues = validateProps({
  commandValues: PropTypes.arrayOf(
    PropTypes.number.isRequired.custom(({prop, propName}) => {
      const command = ORDERED_COMMANDS[prop];
      if (!command) {
        const allowedCommandValues = _.map(ORDERED_COMMANDS, (command, index) => `${index} - ${command}`);
        throw new Error(`Invalid command value ${prop} at commandIndex ${propName} (must be one of [${allowedCommandValues.join(', ')}])`);
      }
    }),
  ).isRequired,
});


const positiveIntegerCustomCheck = ({prop}) => {
  if (prop <= 0 || Math.floor(prop) !== prop) {
    return `must be a positive integer`;
  }
};

const integerCustomCheck = ({prop}) => {
  if (Math.floor(Math.abs(prop)) !== Math.abs(prop)) {
    return `must be an integer`;
  }
};

const createManagedInputError = ({metaErr}) => `Invalid managedValue: ${metaErr.message}`;

const validateManagedInput = validateProps({
  dimensions: PropTypes.shape({
    width: PropTypes.number.isRequired.custom(positiveIntegerCustomCheck),
    height: PropTypes.number.isRequired.custom(positiveIntegerCustomCheck),
  }).isRequired.error(createManagedInputError),
  position: PropTypes.shape({
    x: PropTypes.number.isRequired.custom(integerCustomCheck),
    y: PropTypes.number.isRequired.custom(integerCustomCheck),
  }).isRequired.error(createManagedInputError),
  orientation: PropTypes.oneOf(_.values(ORIENTATIONS)).isRequired.error(createManagedInputError),
  commands: PropTypes.arrayOf(
    PropTypes.oneOf(_.values(COMMAND_INDICES)).isRequired,
  ).isRequired.error(createManagedInputError),
  removals: PropTypes.arrayOf(
    PropTypes.shape({
      x: PropTypes.number.isRequired.custom(integerCustomCheck),
      y: PropTypes.number.isRequired.custom(integerCustomCheck),
    }).isRequired,
  ).isRequired.error(createManagedInputError),
});

// ////
// parser
// ////

const parseInput = (inputRaw, usesHeader) => {
  validateRawInput({inputRaw});
  // extract string items & validate
  const inputList = inputRaw.replace(/^,+|,+$/g, '').split(',');
  validateInputList({inputList});
  // check basic values & partition
  const inputValues = _.map(inputList, (item) => Number(item));
  const numSlice = usesHeader ? 4 : 0;
  const headerValues = inputValues.slice(0, numSlice);
  const commandValues = inputValues.slice(numSlice);
  // process the header
  let dimensions = null;
  let position = null;
  if (usesHeader) {
    validateHeaderValues({headerValues});
    dimensions = {
      width: inputValues[0],
      height: inputValues[1],
    };
    position = {
      x: inputValues[2],
      y: inputValues[3],
    };
  }
  // process the commands
  validateCommandValues({commandValues});
  const commands = _.map(commandValues, (commandValue) => ORDERED_COMMANDS[commandValue]);
  // and finish
  return {
    dimensions,
    position,
    commands,
  };
};

module.exports = {
  validateManagedInput,
  parseInput,
};
