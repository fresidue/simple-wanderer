'use strict';

const _ = require('lodash');
const {v4: uuid} = require('uuid');

const {ORIENTATIONS, ORIENTATION_INDICES} = require('../constants');

const createBoard = ({dimensions}) => {
  // first a flat list of all possible coordinates
  const tiles = [];
  _.times(dimensions.width, (xCoord) => {
    _.times(dimensions.height, (yCoord) => {
      tiles.push({
        uuid: uuid(),
        x: xCoord,
        y: yCoord,
        neighbors: [null, null, null, null], // default
      });
    });
  });
  // then pack it in a board by uuid
  const board = {tiles: {}};
  _.each(tiles, (tile) => {
    board.tiles[tile.uuid] = tile;
  });
  // and then identify neighbors
  _.each(board.tiles, (tile) => {
    const neighborCoordsSet = {
      [ORIENTATIONS.north]: {x: tile.x, y: tile.y - 1},
      [ORIENTATIONS.east]: {x: tile.x + 1, y: tile.y},
      [ORIENTATIONS.south]: {x: tile.x, y: tile.y + 1},
      [ORIENTATIONS.west]: {x: tile.x - 1, y: tile.y},
    };
    _.each(neighborCoordsSet, (neighborCoords, orientation) => {
      const index = ORIENTATION_INDICES[orientation];
      if (tile.neighbors[index]) {
        return; // already been found
      }
      const neighbor = _.find(board.tiles, neighborCoords);
      if (neighbor) {
        tile.neighbors[index] = neighbor.uuid;
        neighbor.neighbors[(index + 2) % 4] = tile.uuid;
      }
    });
  });

  board.boardRotateCW = (transform) => {
    _.each(board.tiles, (tile) => {
      const nextPos = transform(tile);
      tile.x = nextPos.x;
      tile.y = nextPos.y;
      const nextNeighbors = [
        tile.neighbors[3],
        tile.neighbors[0],
        tile.neighbors[1],
        tile.neighbors[2],
      ];
      tile.neighbors = nextNeighbors;
    });
  };
  board.boardRotateCCW = (transform) => {
    _.each(board.tiles, (tile) => {
      const nextPos = transform(tile);
      tile.x = nextPos.x;
      tile.y = nextPos.y;
      const nextNeighbors = [
        tile.neighbors[1],
        tile.neighbors[2],
        tile.neighbors[3],
        tile.neighbors[0],
      ];
      tile.neighbors = nextNeighbors;
    });
  };

  return board;
};

module.exports = {
  createBoard,
};
