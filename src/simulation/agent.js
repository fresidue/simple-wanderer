'use strict';

const _ = require('lodash');

const {DEFAULT_ORIENTATION, ORIENTATION_INDICES, ORDERED_ORIENTATIONS} = require('../constants');

const checkIsAlive = (agent, tiles) => {
  const tile = _.find(tiles, _.pick(agent, ['x', 'y']));
  const isAlive = !!tile;
  return isAlive;
};

const findNextCoords = (agent, isForwards) => {
  const index = ORIENTATION_INDICES[agent.orientation];
  const isPositiveOrientation = index === 1 || index === 2;
  const diffMag = (isPositiveOrientation ? 1 : -1) * (isForwards ? 1 : -1);
  let diffX = 0;
  let diffY = 0;
  if (index % 2 === 1) {
    diffX = diffMag;
  } else {
    diffY = diffMag;
  }
  const nextCoords = {
    x: agent.x + diffX,
    y: agent.y + diffY,
  };
  return nextCoords;
};

const findNextOrientation = (agent, isClockwise) => {
  const index = ORIENTATION_INDICES[agent.orientation];
  const diff = isClockwise ? 1 : -1;
  const nextIndex = (index + 4 + diff) % 4;
  const nextOrientation = ORDERED_ORIENTATIONS[nextIndex];
  return nextOrientation;
};

const createAgent = ({position}) => {
  const agent = {
    x: position.x,
    y: position.y,
    numExecuted: 0,
    orientation: DEFAULT_ORIENTATION,
    isAlive: true,
    hasQuit: false,
    trajectory: [{x: position.x, y: position.y}],
  };
  agent.updateStatus = (tiles) => {
    const isAlive = checkIsAlive(agent, tiles);
    agent.isAlive = isAlive;
  };
  agent.quit = () => {
    agent.hasQuit = true;
    agent.numExecuted += 1;
  };
  agent.moveForward = () => {
    const nextCoords = findNextCoords(agent, true);
    agent.x = nextCoords.x;
    agent.y = nextCoords.y;
    agent.trajectory.push(nextCoords);
    agent.numExecuted += 1;
  };
  agent.moveBackward = () => {
    const nextCoords = findNextCoords(agent, false);
    agent.x = nextCoords.x;
    agent.y = nextCoords.y;
    agent.trajectory.push(nextCoords);
    agent.numExecuted += 1;
  };
  agent.rotateCW = () => {
    const nextOrientation = findNextOrientation(agent, true);
    agent.orientation = nextOrientation;
    agent.numExecuted += 1;
  };
  agent.rotateCCW = () => {
    const nextOrientation = findNextOrientation(agent, false);
    agent.orientation = nextOrientation;
    agent.numExecuted += 1;
  };
  agent.boardRotateCW = (transform) => {
    const nextTrajectory = _.map(agent.trajectory, (pos) => {
      const nextPos = transform(pos);
      return nextPos;
    });
    agent.trajectory = nextTrajectory;
    agent.numExecuted += 1;
  };
  agent.boardRotateCCW = (transform) => {
    const nextTrajectory = _.map(agent.trajectory, (pos) => {
      const nextPos = transform(pos);
      return nextPos;
    });
    agent.trajectory = nextTrajectory;
    agent.numExecuted += 1;
  };
  return agent;
};

module.exports = {
  createAgent,
};
