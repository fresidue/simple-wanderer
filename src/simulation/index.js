'use strict';

const _ = require('lodash');

const {COMMANDS} = require('../constants');
const {parseInput, validateManagedInput} = require('../input-parser');
const {createBoard} = require('./board');
const {createAgent} = require('./agent');

// mutable singleton state
const state = {
  board: null,
  agent: null,
};

const getFilteredAgent = (agent) => {
  const filteredAgent = _.omit(agent, 'updateStatus', ..._.values(COMMANDS));
  // const filteredAgent = _.pick(agent, ['x', 'y', 'orientation', 'numExecuted', 'isAlive', 'hasQuit', 'trajectory']);
  return filteredAgent;
};

const getFilteredBoard = (board) => {
  const filteredBoard = {tiles: _.clone(board.tiles)};
  return filteredBoard;
};

const generateBoardRotationTransform = (board, agent, isCW) => {
  const origin = _.pick(agent, ['x', 'y']);
  const transform = (pos) => {
    const centeredPos = {
      x: pos.x - origin.x,
      y: pos.y - origin.y,
    };
    const rotatedPos = {
      x: (isCW ? -1 : 1) * centeredPos.y,
      y: (isCW ? 1 : -1) * centeredPos.x,
    };
    const finalPos = {
      x: rotatedPos.x + origin.x,
      y: rotatedPos.y + origin.y,
    };
    return finalPos;
  };
  return transform;
};

const runCommands = (commands) => {
  _.each(commands, (command) => {
    switch (command) {
      case COMMANDS.quit: {
        state.agent.quit();
        return false; // early return (hasQuit === true)
      }
      case COMMANDS.moveForward: {
        state.agent.moveForward();
        break;
      }
      case COMMANDS.moveBackward: {
        state.agent.moveBackward();
        break;
      }
      case COMMANDS.rotateCW: {
        state.agent.rotateCW();
        break;
      }
      case COMMANDS.rotateCCW: {
        state.agent.rotateCCW();
        break;
      }
      case COMMANDS.boardRotateCW: {
        const isCW = true;
        const transform = generateBoardRotationTransform(state.board, state.agent, isCW);
        state.board.boardRotateCW(transform);
        state.agent.boardRotateCW(transform);
        break;
      }
      case COMMANDS.boardRotateCCW: {
        const isCW = false;
        const transform = generateBoardRotationTransform(state.board, state.agent, isCW);
        state.board.boardRotateCCW(transform);
        state.agent.boardRotateCCW(transform);
        break;
      }
      default: {
        throw new Error(`DevErr: runSimulation currently not handling COMMAND.${command}`);
      }
    }
    // update status
    state.agent.updateStatus(state.board.tiles);
    if (!state.agent.isAlive) {
      return false; // early return (from each
    }
  });
};

const runSimulation = (commandsRaw) => {
  // initialize & check initial status

  // console.log('commandsRaw:');
  // console.log(commandsRaw);

  let parsedInput;
  if (_.isObject(commandsRaw)) {
    validateManagedInput(commandsRaw);
    parsedInput = commandsRaw;
  } else {
    const usesHeader = true;
    parsedInput = parseInput(commandsRaw, usesHeader);
     // const {dimensions, position, commands}
  }
  console.log({parsedInput});

  state.board = createBoard(_.pick(parsedInput, ['dimensions', 'removals']));
  state.agent = createAgent(_.pick(parsedInput, ['position', 'orientation']));
  state.agent.updateStatus(state.board.tiles);
  if (!state.agent.isAlive) {
    return {
      agent: getFilteredAgent(state.agent),
      board: getFilteredBoard(state.board),
    };
  }
  // run commands
  runCommands(parsedInput.commands);
  // and finish
  return {
    agent: getFilteredAgent(state.agent),
    board: getFilteredBoard(state.board),
  };
};


const updateSimulation = (commandsRaw) => {
  if (!state.agent || !state.board) {
    throw new Error(`DevErr: updateSimulation cannot be extended without first being run`);
  }
  if (!state.agent.isAlive || state.agent.hasQuit) {
    // can't do nuthin
    return {
      agent: getFilteredAgent(state.agent),
      board: getFilteredBoard(state.board),
    };
  }
  const usesHeader = false;
  const {commands} = parseInput(commandsRaw, usesHeader);
  // run commands
  runCommands(commands);
  // and finish
  return {
    agent: getFilteredAgent(state.agent),
    board: getFilteredBoard(state.board),
  };
};

const clearSimulation = () => {
  state.agent = null;
  state.board = null;
};

module.exports = {
  runSimulation,
  updateSimulation,
  clearSimulation,
};
