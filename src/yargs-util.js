'use strict';

const _ = require('lodash');
const yargs = require('yargs');
const {PropTypes, validateProps} = require('vanilla-prop-types');

const ARG_KEYS = {
  interactive: 'interactive',
};

const validateYargs = validateProps({
  yargs: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
  )
    .isRequired
    .error(({metaErr}) => new Error(`Invalid yargs which must be an array of strings and/or integers: ${metaErr.message}`)),
});

const combineYargs = (yargs) => {
  validateYargs({yargs});
  const items = _.flatten(_.map(yargs, (yarg) => {
    const rawItems = String(yarg).split(/[ ,]+/);
    const inputItems = _.compact(rawItems);
    return inputItems;
  }));
  const input = items.join(',');
  return input;
};

const processYargs = () => {
  const argv = yargs
    .option(ARG_KEYS.interactive, {
      alias: 'i',
      description: 'interactive mode',
      type: 'boolean',
    })
    .help()
    .alias('help', 'h')
    .argv;
  const res = {...argv, input: combineYargs(argv._)};
  return res;
};

module.exports = {
  combineYargs,
  processYargs,
};
