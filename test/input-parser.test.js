'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  parseInput,
  validateManagedInput,
} = require('../src/input-parser');

describe('input-parser.test', () => {

  describe('parseInput', () => {

    it('invalids (usesHeader === true)', () => {
      const invalids = [
        {commands: null, mess: /the input must be a non-empty string/},
        {commands: undefined, mess: /the input must be a non-empty string/},
        {commands: 23, mess: /the input must be a non-empty string/},
        {commands: '', mess: /the input must be a non-empty string/},
        {commands: '1', mess: /Invalid header values - there must be exactly 4, but 1 were found/},
        {commands: 'a', mess: /Invalid inputList item "a" at index 0 \(must be an integer\)/},
        {commands: '2.34', mess: /Invalid inputList item "2.34" at index 0 \(must be an integer\)/},
        {commands: '1,1,1,1,a', mess: /Invalid inputList item "a" at index 4 \(must be an integer\)/},
        {commands: '0', mess: /Invalid table width 0 which must be greater than 0/},
        {commands: '1,0', mess: /Invalid table height 0 which must be greater than 0/},
        {commands: '1,1,1,1,-1', mess: /Invalid command value -1 at commandIndex 0 \(must be one of \[0 - quit, 1 - moveForward, 2 - moveBackward, 3 - rotateCW, 4 - rotateCCW, 5 - boardRotateCW, 6 - boardRotateCCW]\)/},
        {commands: '1,1,1,1,7', mess: /Invalid command value 7 at commandIndex 0 \(must be one of \[0 - quit, 1 - moveForward, 2 - moveBackward, 3 - rotateCW, 4 - rotateCCW, 5 - boardRotateCW, 6 - boardRotateCCW]\)/},
      ];
      _.each(invalids, (invalid) => {
        const usesHeader = true;
        assert.throws(() => parseInput(invalid.commands, usesHeader), invalid.mess);
      });
    });

    it('invalids (usesHeader === false)', () => {
      const invalids = [
        {commands: null, mess: /the input must be a non-empty string/},
        {commands: undefined, mess: /the input must be a non-empty string/},
        {commands: 23, mess: /the input must be a non-empty string/},
        {commands: '', mess: /the input must be a non-empty string/},
        {commands: 'a', mess: /Invalid inputList item "a" at index 0 \(must be an integer\)/},
        {commands: '2.34', mess: /Invalid inputList item "2.34" at index 0 \(must be an integer\)/},
        {commands: '1,-1', mess: /Invalid command value -1 at commandIndex 1 \(must be one of \[0 - quit, 1 - moveForward, 2 - moveBackward, 3 - rotateCW, 4 - rotateCCW, 5 - boardRotateCW, 6 - boardRotateCCW]\)/},
        {commands: '1,7', mess: /Invalid command value 7 at commandIndex 1 \(must be one of \[0 - quit, 1 - moveForward, 2 - moveBackward, 3 - rotateCW, 4 - rotateCCW, 5 - boardRotateCW, 6 - boardRotateCCW]\)/},
      ];
      _.each(invalids, (invalid) => {
        assert.throws(() => parseInput(invalid.commands), invalid.mess);
      });
    });

    it('some valids', () => {
      const valids = [
        {
          commands: '1, 1,   34,  -2827, 0  , 1  ,2,3,4',
          result: {
            dimensions: {width: 1, height: 1},
            position: {x: 34, y: -2827},
            commands: ['quit', 'moveForward', 'moveBackward', 'rotateCW', 'rotateCCW'],
          },
          usesHeader: true,
        },
        {
          commands: '1,4',
          result: {
            dimensions: null,
            position: null,
            commands: ['moveForward', 'rotateCCW'],
          },
          usesHeader: false,
        },
      ];
      _.each(valids, (valid) => {
        const res = parseInput(valid.commands, valid.usesHeader);
        assert.deepStrictEqual(res, valid.result);
      });

    });

  });

  describe('validateManagedInput', () => {

    const minimalValid = {
      dimensions: {width: 2, height: 3},
      position: {x: 1, y: 2},
      orientation: 'north',
      commands: [],
      removals: [],
    };
    it('minimalValid is valid', () => {
      assert.doesNotThrow(() => validateManagedInput(minimalValid));
    });

    const maximalValid = {
      ...minimalValid,
      commands: [0, 1, 2, 3, 4, 5, 6],
      removals: [{x: 1, y: 2}, {x: 2, y: -1}],
    };
    it('maximalValid is valid', () => {
      assert.doesNotThrow(() => validateManagedInput(maximalValid));
    });


    it('some invalids', () => {
      const invalids = [
        // boardsize
        {
          invalid: _.omit(minimalValid, 'dimensions'),
          mess: /Invalid managedValue: PropTypes validation error at \[dimensions]: Prop isRequired \(cannot be null or undefined\)/,
        },
        {
          invalid: {...minimalValid, dimensions: {width: 'a', height: 3}},
          mess: /PropTypes validation error at \[dimensions, width]: Prop must be a number when included/,
        },
        {
          invalid: {...minimalValid, dimensions: {width: -1, height: 3}},
          mess: /Invalid managedValue: PropTypes validation error at \[dimensions, width]: must be a positive integer/,
        },
        {
          invalid: {...minimalValid, dimensions: {width: 2.3, height: 3}},
          mess: /Invalid managedValue: PropTypes validation error at \[dimensions, width]: must be a positive integer/,
        },
        {
          invalid: {...minimalValid, dimensions: {width: 2, height: 'b'}},
          mess: /PropTypes validation error at \[dimensions, height]: Prop must be a number when included/,
        },
        {
          invalid: {...minimalValid, dimensions: {width: 2, height: -1}},
          mess: /Invalid managedValue: PropTypes validation error at \[dimensions, height]: must be a positive integer/,
        },
        {
          invalid: {...minimalValid, dimensions: {width: 2, height: 3.2}},
          mess: /Invalid managedValue: PropTypes validation error at \[dimensions, height]: must be a positive integer/,
        },
        // position
        {
          invalid: _.omit(minimalValid, 'position'),
          mess: /Invalid managedValue: PropTypes validation error at \[position]: Prop isRequired \(cannot be null or undefined\)/,
        },
        {
          invalid: {...minimalValid, position: {x: 'a', y: 2}},
          mess: /Invalid managedValue: PropTypes validation error at \[position, x]: Prop must be a number when included/,
        },
        {
          invalid: {...minimalValid, position: {x: 1.2, y: 2}},
          mess: /Invalid managedValue: PropTypes validation error at \[position, x]: must be an integer/,
        },
        {
          invalid: {...minimalValid, position: {x: 1, y: 'b'}},
          mess: /PropTypes validation error at \[position, y]: Prop must be a number when included/,
        },
        {
          invalid: {...minimalValid, position: {x: 1, y: 2.1}},
          mess: /Invalid managedValue: PropTypes validation error at \[position, y]: must be an integer/,
        },
        // orientation
        {
          invalid: _.omit(minimalValid, 'orientation'),
          mess: /Invalid managedValue: PropTypes validation error at \[orientation]: Prop isRequired \(cannot be null or undefined\)/,
        },
        {
          invalid: {...minimalValid, orientation: 'anyotherstring..'},
          mess: /Invalid managedValue: PropTypes validation error at \[orientation]: Prop must be contained in enum: \[north, east, south, ...]/,
        },
        // commands
        {
          invalid: _.omit(minimalValid, 'commands'),
          mess: /Invalid managedValue: PropTypes validation error at \[commands]: Prop isRequired \(cannot be null or undefined\)/,
        },
        {
          invalid: {...minimalValid, commands: 'must be an array'},
          mess: /Invalid managedValue: PropTypes validation error at \[commands]: Prop must be an array \(PropTypes.arrayOf requirement\)/,
        },
        {
          invalid: {...minimalValid, commands: ['3']},
          mess: /Invalid managedValue: PropTypes validation error at \[commands, 0]: Prop must be contained in enum: \[0, 1, 2, ...]/,
        },
        {
          invalid: {...minimalValid, commands: [0, 2, -1]},
          mess: /Invalid managedValue: PropTypes validation error at \[commands, 2]: Prop must be contained in enum: \[0, 1, 2, ...]/,
        },
        {
          invalid: {...minimalValid, commands: [0, 1, 2, 3, 4, 5, 6, 7]},
          mess: /Invalid managedValue: PropTypes validation error at \[commands, 7]: Prop must be contained in enum: \[0, 1, 2, ...]/,
        },
        // removals
        {
          invalid: _.omit(minimalValid, 'removals'),
          mess: /Invalid managedValue: PropTypes validation error at \[removals]: Prop isRequired \(cannot be null or undefined\)/,
        },
        {
          invalid: {...minimalValid, removals: 'not_an_array'},
          mess: /Invalid managedValue: PropTypes validation error at \[removals]: Prop must be an array \(PropTypes.arrayOf requirement\)/,
        },
        {
          invalid: {...minimalValid, removals: [{x: 0, y: 0}, {x: 'a', y: 2}]},
          mess: /Invalid managedValue: PropTypes validation error at \[removals, 1, x]: Prop must be a number when included/,
        },
        {
          invalid: {...minimalValid, removals: [{x: 0, y: 0}, {x: 1.2, y: 2}]},
          mess: /Invalid managedValue: PropTypes validation error at \[removals, 1, x]: must be an integer/,
        },
        {
          invalid: {...minimalValid, removals: [{x: 0, y: 0}, {x: 1, y: 'b'}]},
          mess: /PropTypes validation error at \[removals, 1, y]: Prop must be a number when included/,
        },
        {
          invalid: {...minimalValid, removals: [{x: 0, y: 0}, {x: 1, y: 2.1}]},
          mess: /Invalid managedValue: PropTypes validation error at \[removals, 1, y]: must be an integer/,
        },
      ];
      _.each(invalids, invalid => {
        assert.throws(() => validateManagedInput(invalid.invalid), invalid.mess);
      });
    });

  });

});
