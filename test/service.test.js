'use strict';

const assert = require('assert');
const delay = require('delay');

const {start, stop} = require('../src/index');

describe('service.test', () => {

  after(async () => {
    await stop();
  });

  it('start server with DEATH commands', async () => {
    const res = await start('1,1,0,0,1');
    assert.deepStrictEqual(res, [-1, -1]);
  });

  it('start server with QUIT command', async () => {
    const res = await start('1,1,0,0,0');
    assert.deepStrictEqual(res, [0, 0]);
  });

  it('start server with it still alive and kicking (times out)', async () => {
    let flag = false;
    // start WITHOUT await (sim gets hung up waiting)
    start('1,1,0,0,3,3').then(() => {
      flag = true;
    });
    await delay(500);
    assert.strictEqual(flag, false);
  });

});
