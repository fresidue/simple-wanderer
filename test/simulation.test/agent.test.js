'use strict';

const _ = require('lodash');
const assert = require('assert');
const {createAgent} = require('../../src/simulation/agent');

describe('agent.test', () => {

  it('a typical valid', () => {
    const position = {x: 234, y: -23424};
    const agent = createAgent({position});
    assert(_.isFunction(agent.updateStatus));
    assert(_.isFunction(agent.quit));
    assert(_.isFunction(agent.moveForward));
    assert(_.isFunction(agent.moveBackward));
    assert(_.isFunction(agent.rotateCW));
    assert(_.isFunction(agent.rotateCCW));
    assert(_.isFunction(agent.boardRotateCW));
    assert(_.isFunction(agent.boardRotateCCW));

    assert.deepStrictEqual(agent, {
      ..._.pick(agent, [
        'updateStatus',
        'quit',
        'moveForward',
        'moveBackward',
        'rotateCW',
        'rotateCCW',
        'boardRotateCW',
        'boardRotateCCW',
      ]),
      ...position,
      orientation: 'north',
      numExecuted: 0,
      isAlive: true,
      hasQuit: false,
      trajectory: [{x: 234, y: -23424}],
    });
  });

});
