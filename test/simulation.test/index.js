'use strict';

describe('simulation.test', () => {

  require('./board.test');
  require('./agent.test');
  require('./run-simulation.test');

});
