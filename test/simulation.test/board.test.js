'use strict';

const _ = require('lodash');
const assert = require('assert');
const uuid = require('uuid');
const {createBoard} = require('../../src/simulation/board');

describe('board.test', () => {

  it('1 X 1', () => {
    const dimensions = {width: 1, height: 1};
    const board = createBoard({dimensions});
    const boardKeys = ['boardRotateCCW', 'boardRotateCW', 'tiles'];
    assert.deepStrictEqual(_.sortBy(_.keys(board)), boardKeys);
    assert.strictEqual(_.size(board.tiles), 1);
    const tile = _.values(board.tiles).pop();
    assert.strictEqual(uuid.validate(tile.uuid), true);
    assert.deepStrictEqual(tile, {
      uuid: tile.uuid,
      x: 0,
      y: 0,
      neighbors: [null, null, null, null],
    });
  });

  it('2 X 2', () => {
    const dimensions = {width: 2, height: 2};
    const board = createBoard({dimensions});
    assert.strictEqual(_.size(board.tiles), 4);
    _.each(board.tiles, (tile) => {
      assert(_.size(_.compact(tile.neighbors)), 2); // 2 are always null
    });
    // identify tiles
    const tileNW = _.find(board.tiles, {x: 0, y: 0});
    const tileNE = _.find(board.tiles, {x: 1, y: 0});
    const tileSE = _.find(board.tiles, {x: 1, y: 1});
    const tileSW = _.find(board.tiles, {x: 0, y: 1});
    // and make sure neighbors look right
    assert.strictEqual(tileNW.neighbors[1], tileNE.uuid);
    assert.strictEqual(tileNW.neighbors[2], tileSW.uuid);
    assert.strictEqual(tileNE.neighbors[2], tileSE.uuid);
    assert.strictEqual(tileNE.neighbors[3], tileNW.uuid);
    assert.strictEqual(tileSE.neighbors[0], tileNE.uuid);
    assert.strictEqual(tileSE.neighbors[3], tileSW.uuid);
    assert.strictEqual(tileSW.neighbors[0], tileNW.uuid);
    assert.strictEqual(tileSW.neighbors[1], tileSE.uuid);
  });

  it('4 X 7', () => {
    const dimensions = {width: 4, height: 7};
    const board = createBoard({dimensions});
    assert.strictEqual(_.size(board.tiles), 28);
    // there should be zero with 0 neighbors
    const lonelies = _.filter(board.tiles, (tile) => _.size(_.compact(tile.neighbors)) === 0);
    assert.strictEqual(lonelies.length, 0);
    // there should be zero with 1 neighbor
    const singles = _.filter(board.tiles, (tile) => _.size(_.compact(tile.neighbors)) === 1);
    assert.strictEqual(singles.length, 0);
    // there should be 4 with 2 neighbors (corners)
    const doubles = _.filter(board.tiles, (tile) => _.size(_.compact(tile.neighbors)) === 2);
    assert.strictEqual(doubles.length, 4);
    // there should be 14 with 3 neighbors (edges)
    const triples = _.filter(board.tiles, (tile) => _.size(_.compact(tile.neighbors)) === 3);
    assert.strictEqual(triples.length, 14);
    // remaining 10 should have 4 neighbors (interior)
    const fullies = _.filter(board.tiles, (tile) => _.size(_.compact(tile.neighbors)) === 4);
    assert.strictEqual(fullies.length, 10);
  });

});
