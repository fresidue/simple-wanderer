'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  runSimulation,
  updateSimulation,
  clearSimulation,
} = require('../../src/simulation');

describe('run-simulation.test', () => {

  it('1x1 table, placed OFF table, no commands', () => {
    const commands = '1,1,1,1';
    const res = runSimulation(commands);
    assert.deepStrictEqual(_.sortBy(_.keys(res)), ['agent', 'board']);
    assert.deepStrictEqual(res.agent, {
      x: 1,
      y: 1,
      orientation: 'north',
      numExecuted: 0,
      hasQuit: false,
      isAlive: false, // DEAD
      trajectory: res.agent.trajectory,
    });
    assert.strictEqual(_.size(res.agent.trajectory), 1);
    assert.deepStrictEqual(res.agent.trajectory[0], _.pick(res.agent, ['x', 'y']));
  });

  it('1x1 table, placed on table, no commands', () => {
    const commands = '1,1,0,0';
    const res = runSimulation(commands);
    assert.deepStrictEqual(res.agent, {
      x: 0,
      y: 0,
      orientation: 'north',
      numExecuted: 0,
      hasQuit: false,
      isAlive: true,
      trajectory: res.agent.trajectory,
    });
  });

  it('1x1 table, placed on table, 1 quit command', () => {
    const commands = '1,1,0,0,0';
    const res = runSimulation(commands);
    assert.deepStrictEqual(res.agent, {
      x: 0,
      y: 0,
      orientation: 'north',
      numExecuted: 1,
      hasQuit: true,
      isAlive: true,
      trajectory: res.agent.trajectory,
    });
  });

  it('1x1, placed on table, only rotations', () => {
    const valids = [
      {commands: '1,1,0,0,3', orientation: 'east', numExecuted: 1},
      {commands: '1,1,0,0,3,3', orientation: 'south', numExecuted: 2},
      {commands: '1,1,0,0,3,3,3', orientation: 'west', numExecuted: 3},
      {commands: '1,1,0,0,3,3,3,3', orientation: 'north', numExecuted: 4},
      {commands: '1,1,0,0,4', orientation: 'west', numExecuted: 1},
      {commands: '1,1,0,0,4,4', orientation: 'south', numExecuted: 2},
      {commands: '1,1,0,0,4,4,4', orientation: 'east', numExecuted: 3},
      {commands: '1,1,0,0,4,4,4,4', orientation: 'north', numExecuted: 4},
    ];
    _.each(valids, (valid) => {
      const res = runSimulation(valid.commands);
      assert.strictEqual(res.agent.orientation, valid.orientation);
      assert.strictEqual(res.agent.numExecuted, valid.numExecuted);
    });
  });

  it('1x1, moving forwards (also try backwards) in all directions', () => {
    const valids = [
      {commands: '1,1,0,0,1', orientation: 'north', numExecuted: 1, x: 0, y: -1},
      {commands: '1,1,0,0,3,1', orientation: 'east', numExecuted: 2, x: 1, y: 0},
      {commands: '1,1,0,0,3,3,1', orientation: 'south', numExecuted: 3, x: 0, y: 1},
      {commands: '1,1,0,0,3,3,3,1', orientation: 'west', numExecuted: 4, x: -1, y: 0},
      {commands: '1,1,0,0,2', orientation: 'north', numExecuted: 1, x: 0, y: 1},
      {commands: '1,1,0,0,3,2', orientation: 'east', numExecuted: 2, x: -1, y: 0},
      {commands: '1,1,0,0,3,3,2', orientation: 'south', numExecuted: 3, x: 0, y: -1},
      {commands: '1,1,0,0,3,3,3,2', orientation: 'west', numExecuted: 4, x: 1, y: 0},
    ];
    _.each(valids, (valid) => {
      const res = runSimulation(valid.commands);
      const expected = {
        ..._.pick(valid, ['orientation', 'numExecuted', 'x', 'y']),
        isAlive: false,
        hasQuit: false,
        trajectory: res.agent.trajectory,
      };
      assert.deepStrictEqual(res.agent, expected);
      // also try reversing and check that additional move does not change anything
      const lastCommand = Number(valid.commands.split(',').pop());
      const reverseCommand = (lastCommand % 2) + 1;
      const res2 = runSimulation(`${valid.commands},${reverseCommand}`);
      assert.deepStrictEqual(res2.agent, res.agent);
    });
  });

  it('2,7,0,0 can only go so far in each direction', () => {
    const ones = _.times(100, () => 1).join(',');
    const valids = [
      {commands: `2,7,0,0,${ones}`, orientation: 'north', numExecuted: 1, x: 0, y: -1},
      {commands: `2,7,0,0,3,${ones}`, orientation: 'east', numExecuted: 3, x: 2, y: 0},
      {commands: `2,7,0,0,3,3,${ones}`, orientation: 'south', numExecuted: 9, x: 0, y: 7},
      {commands: `2,7,0,0,3,3,3,${ones}`, orientation: 'west', numExecuted: 4, x: -1, y: 0},
    ];
    _.each(valids, (valid) => {
      const res = runSimulation(valid.commands);
      const expected = {
        ..._.pick(valid, ['orientation', 'numExecuted', 'x', 'y']),
        isAlive: false,
        hasQuit: false,
        trajectory: res.agent.trajectory,
      };
      assert.deepStrictEqual(res.agent, expected);
    });
  });

  it('"4,4,2,2" with commands "1,4,1,3,2,3,2,4,1,0" example', () => {
    const res = runSimulation('4,4,2,2,1,4,1,3,2,3,2,4,1,0');
    const expected = {
      x: 0,
      y: 1,
      orientation: 'north',
      numExecuted: 10,
      isAlive: true,
      hasQuit: true,
      trajectory: res.agent.trajectory,
    };
    assert.deepStrictEqual(res.agent, expected);
  });


  describe('boardRotate', () => {

    describe('2 X 1 all rotations, pos = {0, 0}', () => {

      it('no rotation', () => {
        const res = runSimulation('2,1,0,0');
        assert.strictEqual(res.agent.numExecuted, 0);
        const tile0 = _.find(res.board.tiles, {x: 0, y: 0});
        const tile1 = _.find(res.board.tiles, {x: 1, y: 0});
        assert.strictEqual(tile0.neighbors[1], tile1.uuid);
        assert.strictEqual(tile1.neighbors[3], tile0.uuid);
      });

      it('1 boardRotateCW', () => {
        const res = runSimulation('2,1,0,0,5');
        assert.strictEqual(res.agent.numExecuted, 1);
        const tile0 = _.find(res.board.tiles, {x: 0, y: 0});
        const tile1 = _.find(res.board.tiles, {x: 0, y: 1});
        assert.strictEqual(tile0.neighbors[2], tile1.uuid);
        assert.strictEqual(tile1.neighbors[0], tile0.uuid);
      });

      it('1 boardRotateCCW', () => {
        const res = runSimulation('2,1,0,0,6');
        assert.strictEqual(res.agent.numExecuted, 1);
        const tile0 = _.find(res.board.tiles, {x: 0, y: 0});
        const tile1 = _.find(res.board.tiles, {x: 0, y: -1});
        assert.strictEqual(tile0.neighbors[0], tile1.uuid);
        assert.strictEqual(tile1.neighbors[2], tile0.uuid);
      });

      it('2 boardRotateCW === 2 boardRotateCCW', () => {
        const resCW = runSimulation('2,1,0,0,5,5');
        const resCCW = runSimulation('2,1,0,0,6,6');
        _.each([resCW, resCCW], (res) => {
          assert.strictEqual(res.agent.numExecuted, 2);
          const tile0 = _.find(res.board.tiles, {x: 0, y: 0});
          const tile1 = _.find(res.board.tiles, {x: -1, y: 0});
          assert.strictEqual(tile0.neighbors[3], tile1.uuid);
          assert.strictEqual(tile1.neighbors[1], tile0.uuid);
        });
      });

    });

    describe('5 X 1, pos = {4, 0}', () => {

      it('no rotation (4CW, 4CCW)', () => {
        const res0 = runSimulation('5,1,4,0');
        const res1 = runSimulation('5,1,4,0,5,5,5,5');
        const res2 = runSimulation('5,1,4,0,6,6,6,6');
        _.each([res0, res1, res2], (res) => {
          const tile0 = _.find(res.board.tiles, {x: 0, y: 0});
          assert(tile0);
          const tile1 = _.find(res.board.tiles, {x: 4, y: 0});
          assert(tile1);
        });
      });

      it('right (1CW, 3CCW)', () => {
        const res0 = runSimulation('5,1,4,0,5');
        const res1 = runSimulation('5,1,4,0,6,6,6');
        _.each([res0, res1], (res) => {
          const tile0 = _.find(res.board.tiles, {x: 4, y: -4});
          assert(tile0);
          const tile1 = _.find(res.board.tiles, {x: 4, y: 0});
          assert(tile1);
        });
      });

      it('left (3CW, 1CCW)', () => {
        const res0 = runSimulation('5,1,4,0,5,5,5');
        const res1 = runSimulation('5,1,4,0,6');
        _.each([res0, res1], (res) => {
          const tile0 = _.find(res.board.tiles, {x: 4, y: 4});
          assert(tile0);
          const tile1 = _.find(res.board.tiles, {x: 4, y: 0});
          assert(tile1);
        });
      });

      it('down (2CW, 2CCW)', () => {
        const res0 = runSimulation('5,1,4,0,5,5');
        const res1 = runSimulation('5,1,4,0,6,6');
        _.each([res0, res1], (res) => {
          const tile0 = _.find(res.board.tiles, {x: 8, y: 0});
          assert(tile0);
          const tile1 = _.find(res.board.tiles, {x: 4, y: 0});
          assert(tile1);
        });
      });

    });

  });

  describe('updateSimulation', () => {

    it('clearSimulation & updateSimulation -> FAIL', () => {
      clearSimulation();
      const mess = /DevErr: updateSimulation cannot be extended without first being run/;
      assert.throws(() => updateSimulation('1,2,3'), mess);
    });

    it('initialize with "4,4,2,2" & updateSimulation with "1,4,1,3,2,3,2,4,1,0"', () => {
      const res1 = runSimulation('4,4,2,2');
      const expected1 = {
        x: 2,
        y: 2,
        orientation: 'north',
        numExecuted: 0,
        isAlive: true,
        hasQuit: false,
        trajectory: res1.agent.trajectory,
      };
      assert.deepStrictEqual(res1.agent, expected1);
      const res2 = updateSimulation('1,4,1,3,2,3,2,4,1,0');
      const expected2 = {
        x: 0,
        y: 1,
        orientation: 'north',
        numExecuted: 10,
        isAlive: true,
        hasQuit: true,
        trajectory: res1.agent.trajectory,
      };
      assert.deepStrictEqual(res2.agent, expected2);
    });

    it('"4,4,2,2" with commands "1,4,1,3,2,3,2,4" and "1,0" multi-update', () => {
      const res1 = runSimulation('4,4,2,2');
      const expected1 = {
        x: 2,
        y: 2,
        orientation: 'north',
        numExecuted: 0,
        isAlive: true,
        hasQuit: false,
        trajectory: res1.agent.trajectory,
      };
      assert.deepStrictEqual(res1.agent, expected1);
      const res2 = updateSimulation('1,4,1,3,2,');
      const expected2 = {
        x: 1,
        y: 2,
        orientation: 'north',
        numExecuted: 5,
        isAlive: true,
        hasQuit: false,
        trajectory: res2.agent.trajectory,
      };
      assert.deepStrictEqual(res2.agent, expected2);
      const res3 = updateSimulation('3,2,4,1,0');
      const expected3 = {
        x: 0,
        y: 1,
        orientation: 'north',
        numExecuted: 10,
        isAlive: true,
        hasQuit: true,
        trajectory: res3.agent.trajectory,
      };
      assert.deepStrictEqual(res3.agent, expected3);
    });

  });

});
