'use strict';

const _ = require('lodash');
const assert = require('assert');

const {combineYargs, processYargs} = require('../src/yargs-util');

describe('yargs-util.test', () => {

  describe('combineYargs', () => {

    it('some invalids', () => {
      const invalids = [
        {yargs: null, mess: /Invalid yargs which must be an array of strings and\/or integers: PropTypes validation error at \[yargs]: Prop isRequired \(cannot be null or undefined\)/},
        {yargs: 'eep', mess: /Invalid yargs which must be an array of strings and\/or integers: PropTypes validation error at \[yargs]: Prop must be an array \(PropTypes.arrayOf requirement\)/},
        {yargs: [true], mess: /Invalid yargs which must be an array of strings and\/or integers: PropTypes validation error at \[yargs, 0]: Prop must be one of 2 given PropTypes \(PropTypes.oneOfType requirement\)/},
      ];
      _.each(invalids, (invalid) => {
        assert.throws(() => combineYargs(invalid.yargs), invalid.mess);
      });
    });

    it('some valids', () => {
      const valids = [
        {yargs: ['1 2 3'], res: '1,2,3'},
        {yargs: ['', '1 2 3', '4,5 6, 7,,,8'], res: '1,2,3,4,5,6,7,8'},
      ];
      _.each(valids, (valid) => {
        const res = combineYargs(valid.yargs);
        assert.strictEqual(res, valid.res);
      });
    });

  });

  describe('processYargs', () => {

    it('current yargs', () => {
      const yargs = processYargs();
      assert.deepStrictEqual(_.omit(yargs, ['watch', 'exit']), {
        _: ['test/index'],
        $0: 'node_modules/.bin/mocha',
        input: 'test/index',
      });
    });

  });

});
