# simple-wanderer

creates a space and wanders around accordingly. pretty simply.

## install
```sh
yarn install
```
or
```sh
npm install
```

## run-wanderer (classic mode)
to run the wanderer according to the original specs, just run `run-wanderer.js` with appropriate yargs (currently comma-delimited and/or white-space delimited), the following will give equivalent results (standing in the root of `simple-wanderer`)
```
/simple-wanderer$ ./run-wanderer.js 4,4,2,2
1,4,1,3,2,3,2,4,1,0       <---- input
0,1                       <---- output
```
```
/simple-wanderer$ ./run-wanderer.js 4,4,2,2
1,4,1,3,2                 <---- input
3,2,4,1,0                 <---- input
0,1                       <---- output
```

```
/simple-wanderer$ ./run-wanderer.js 4,4,2,2,1,4,1,3,2,3,2,4,1,0
0,1                       <---- output
```
The first four values (always required) denot [width, height, posX, posY],
and the rest are commands where (the original commands)
  - 0 -> quit
  - 1 -> moves agent 1 forward
  - 2 -> moves agent 1 backward
  - 3 -> rotates agent 90 degrees CW
  - 4 -> rotates agent 90 degrees CCW

and some added commands
  - 5 -> rotate board 90 degrees CW around agent
  - 6 -> rotate board 90 degrees CCW around agent

## interactive mode (still a work in progress)

 - use the flag `-i` triggers interactive mode
 - currently it must be initialized with a valid command, e.g. `'4,4,2,2'` or `'4,4,2,2,1,2,3,4,5,6'`,

```
/simple-wanderer$ ./run-wanderer.js -i 4,4,2,2,3,3
```
Then either click the red buttons to navigate, or press a keyboard key:
- [0] -> quit
- [1, up] -> moves agent 1 forward
- [2, down] -> moves agent 1 backward
- [3, right] -> rotates agent 90 degrees CW
- [4, left] -> rotates agent 90 degrees CCW
- [5, s] -> rotate board 90 degrees CW around agent
- [6, a] -> rotate board 90 degrees CCW around agent

To quit interactive mode, hit `escape`, `q`, or `Ctrl-c`.

## testing & development

to test, stand in the root dir and
```
$ yarn test
or
$ npm run test
```
when developing, auto-testing on file-save available with
```
$ yarn test-watch
or
$ npm run test-watch
```
