#!/usr/bin/env node
'use strict';

const _ = require('lodash');
const chalk = require('chalk');
const {
  processYargs,
  start,
  startInteractive,
} = require('./src');

(async () => {
  // either case
  const yargs = processYargs();

  try {
    if (yargs.interactive) {
      await startInteractive(yargs.input);
    } else {
      const position = await start(yargs.input);
      process.stdout.write(`${position[0]},${position[1]}\n`);
      process.exit(0);
    }
  } catch (err) {
    process.stderr.write(chalk.red(`\nInternal error running simple-wanderer: ${err}\n`));
    const errStackRaw = err.stack.split('\n');
    errStackRaw.shift(); // get rid of (redundant) first line
    const errStack = _.map(errStackRaw, stackItemRaw => {
      const parts = _.compact(_.split(stackItemRaw, ' '));
      const stackItem = `${chalk.green(parts[0])} ${parts[1]} ${chalk.blue(parts.slice(2).join(' '))}`;
      return stackItem;
    });
    console.log(errStack.join('\n'), '\n\n');
    process.exit(1);
  }

})();
